package vts.app.china.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "qr")
data class QrCodeEntity (@PrimaryKey(autoGenerate = true) var id: Int,
                         @ColumnInfo(name = "qr_result") var qr_result: String)