package vts.app.china.database

import androidx.room.*

@Dao
interface  PhotoDao {
    @Query("SELECT * FROM photo")
    fun getAll(): List<PhotoEntity>

    @Insert
    fun insertAll(vararg todo: PhotoEntity)

    @Query("DELETE  FROM photo WHERE id LIKE :id")
    fun deleteById(id: Int)


    @Query("DELETE  FROM photo")
    fun getDeletAll()
}