package vts.app.china.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photo")
data class PhotoEntity (@PrimaryKey(autoGenerate = true)
                         var id: Int,
                        @ColumnInfo(name = "photo_url") var photo_url: String)