package vts.app.china.model.request

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.create
import vts.app.china.model.response.WareHouseData
import vts.app.china.vtsservice.ApiClient
import vts.app.china.vtsservice.ApiInterface
import vts.app.china.vtsview.WareHouseInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class WareHouseModel(var context: Context) : WareHouseInterface.warehouseModel {

    override fun onSuccessModel(responseData: WareHouseData) {
        warehouseModel?.onSuccessModel(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        warehouseModel?.onErrorModel(throwable)
    }

    private var warehouseModel: WareHouseInterface.warehouseModel? = null

    fun onRequestWarehouseData(warehouseModel: WareHouseInterface.warehouseModel) {

        this.warehouseModel = warehouseModel

        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        apiClient.warehoustList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccessModel, this::onErrorModel)

    }

    fun onLoadWarehouseReport(
        warehouseModel: WareHouseInterface.warehouseModel,
        clear: String = ""
    ) {

        this.warehouseModel = warehouseModel

        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        if (clear.isNullOrBlank()) {
            apiClient.warehouseReportList(clear)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessModel, this::onErrorModel)
            return
        }
        apiClient.warehouseReportClearList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccessModel, this::onErrorModel)

    }
}