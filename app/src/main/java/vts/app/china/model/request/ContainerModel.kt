package vts.app.china.model.request

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.create
import vts.app.china.model.response.ContainerResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.vtsservice.ApiClient
import vts.app.china.vtsservice.ApiInterface
import vts.app.china.vtsview.ContainerInterface
import vts.app.china.vtsview.ReportListInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ContainerModel(var context: Context) : ContainerInterface.containerModel {


    private var mContainerModel: ContainerInterface.containerModel? = null

    // laod report container

    fun onLoadContainerReportList(
        mContainerModel: ContainerInterface.containerModel,
        ware_house_title: String, page: String, isMore: Boolean
    ) {
        this.mContainerModel = mContainerModel
        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        if (!isMore) {
            apiClient.getContainerReportList(
                ware_house_title = "$ware_house_title",
                page = page
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessModel, this::onErrorModel)
            return
        }

        apiClient.getContainerReportList(
            ware_house_title = "$ware_house_title",
            page = page
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccessModelMore, this::onErrorModel)
    }

    // load container list
    fun onLoadContainerList(
        mContainerModel: ContainerInterface.containerModel,
        ware_house_title: String, page: String, isMore: Boolean
    ) {
        this.mContainerModel = mContainerModel
        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        if (!isMore) {
            apiClient.getContainerList(
                ware_house_title = "$ware_house_title",
                page = page
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessModel, this::onErrorModel)
            return
        }

        apiClient.getContainerList(
            ware_house_title = "$ware_house_title",
            page = page
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccessModelMore, this::onErrorModel)

    }

    override fun onSuccessModelMore(responseData: ContainerResponse) {
        mContainerModel?.onSuccessModelMore(responseData)
    }

    override fun onSuccessModel(responseData: ContainerResponse) {
        mContainerModel?.onSuccessModel(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        mContainerModel?.onErrorModel(throwable)
    }

}