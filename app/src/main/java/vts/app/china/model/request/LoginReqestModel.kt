package vts.app.china.model.request

import android.content.Context
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.create
import vts.app.china.model.response.LoginResponse
import vts.app.china.utils.PrefUtils
import vts.app.china.vtsservice.ApiClient
import vts.app.china.vtsservice.ApiInterface
import vts.app.china.vtsview.LoginInterface

class LoginRequestModel(var context: Context) {

    interface OnLoginFinishedListener {
        fun onSuccess(data: LoginResponse)
        fun onError(throwable: Any)
    }

    var mOnLoginFinishedListener: OnLoginFinishedListener? = null

    fun reqestLogin(loginModel: OnLoginFinishedListener, userName: String, password: String) {
        mOnLoginFinishedListener = loginModel
        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        apiClient.loginApi(userName, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccess, this::onError)
    }

    fun onSuccess(data: LoginResponse) {
        mOnLoginFinishedListener?.onSuccess(data)
    }

    fun onError(throwable: Any) {
        mOnLoginFinishedListener?.onError(throwable)
    }

}