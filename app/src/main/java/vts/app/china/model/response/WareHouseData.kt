package vts.app.china.model.response

import java.io.Serializable

/**
 * Created By Taing Sunry on 2020-02-20.
 */

data class WareHouseData(var group_nocode: ArrayList<WareHouseItems> = arrayListOf()) : Serializable

data class WareHouseItems(var ware_house_title: String, var numbers: String):Serializable