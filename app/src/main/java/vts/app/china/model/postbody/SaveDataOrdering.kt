package vts.app.china.model.postbody

import vts.app.china.database.PhotoEntity
import java.io.Serializable

/**
 * Created By Taing Sunry on 2020-02-03.
 */

data class SaveDataOrdering(
    var con_id: String="",
    var con_no_id: String="",
    var customer_name: String = "",
    var receip_no: ArrayList<String>,
    var remark:String,
    var kind_of_goods: String = "",
    var quantity: String = "",
    var userfile: ArrayList<PhotoEntity>
) : Serializable