package vts.app.china.model.response

import java.io.Serializable

/**
 * Created By Taing Sunry on 2020-02-20.
 */

data class ProductResponse(var get_list: ArrayList<ProductData> = arrayListOf()) : Serializable

data class ProductData(
    var con_id: String = "",
    var code_id:String="",
    var con_no_id: String = "",
    var ware_house_title: String,
    var loading_date: String,
    var customer_name: String,
    var kind_of_goods: String,
    var length: String,
    var width: String,
    var height: String,
    var quantity: String,
    var cbm_m3: String,
    var weight: String,
    var remark: String,
    var title: String,
    var delivery: String,
    var debit_code: String
) : Serializable

data class ContainerResponse(var get_list: ArrayList<ContainerData> = arrayListOf()) : Serializable
data class ContainerData(
    var numbers: String = "",
    var con_no_id: String = "",
    var ware_house_title: String = "",
    var loading_date: String = "",
    var title: String = "",
    var selected_items:Boolean = false
):Serializable