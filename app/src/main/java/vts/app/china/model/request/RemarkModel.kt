package vts.app.china.model.request

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.create
import vts.app.china.model.response.RemarkResponse
import vts.app.china.model.response.ReportListData
import vts.app.china.model.response.WareHouseData
import vts.app.china.utils.PrefUtils
import vts.app.china.vtsservice.ApiClient
import vts.app.china.vtsservice.ApiInterface
import vts.app.china.vtsview.RemarkInterface
import vts.app.china.vtsview.WareHouseInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class RemarkModel(var context: Context) : RemarkInterface.remarkModel {

    override fun onSuccessModel(responseData: RemarkResponse) {
        remarkModel?.onSuccessModel(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        remarkModel?.onErrorModel(throwable)
    }

    private var remarkModel: RemarkInterface.remarkModel? = null
    private var userName: String = ""

    fun onAddRemark(remarkModel: RemarkInterface.remarkModel, remark: String, id: String) {
        this.remarkModel = remarkModel
        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()

        if (PrefUtils.getLoginData(context) != null && PrefUtils.getLoginData(context)?.username != null) {
            userName = "${PrefUtils.getLoginData(context)?.username}"
        }

        apiClient.saveRemark("$remark (${userName})", "$id")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccessModel, this::onErrorModel)

    }

}