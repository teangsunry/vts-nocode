package vts.app.china.model.request

import android.content.Context
import com.google.gson.Gson
import com.google.gson.JsonIOException
import com.google.gson.JsonNull
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.create
import vts.app.china.model.postbody.SaveDataOrdering
import vts.app.china.model.response.SaveDataResponse
import vts.app.china.utils.FileHelper
import vts.app.china.vtsservice.ApiClient
import vts.app.china.vtsservice.ApiInterface
import java.io.File
import retrofit2.HttpException
import okhttp3.*
import org.json.JSONException
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import vts.app.china.utils.PrefUtils


/**
 * Created By Taing Sunry on 2020-02-03.
 */

class SaveDataReqestModel(var mContext: Context) {

    interface OnSaveDataFinishedListener {
        fun onSaveSuccess(data: SaveDataResponse)
        fun onSaveError(throwable: Any)
    }

    var mOnSaveDataFinishedListener: OnSaveDataFinishedListener? = null
    fun onSaveData(
        mSaveData: SaveDataOrdering,
        mOnSaveDataFinishedListeners: OnSaveDataFinishedListener
    ) {
        mOnSaveDataFinishedListener = mOnSaveDataFinishedListeners
        val apiClient = ApiClient().getClient(context = mContext).create<ApiInterface>()
        var mDataBody = ArrayList<MultipartBody.Part>()
        if (PrefUtils.getLoginData(mContext) != null && !PrefUtils.getLoginData(mContext)?.username.isNullOrBlank()) {
            mDataBody.add(
                MultipartBody.Part.createFormData(
                    "user",
                    "${PrefUtils.getLoginData(mContext)?.username}"
                )
            )
        }
        mDataBody.add(MultipartBody.Part.createFormData("con_id", "${mSaveData.con_id}"))
        mDataBody.add(
            MultipartBody.Part.createFormData(
                "con_no_id",
                "${mSaveData.con_no_id}"
            )
        )
        mDataBody.add(
            MultipartBody.Part.createFormData(
                "customer_name",
                "${mSaveData.customer_name}"
            )
        )

        mDataBody.add(MultipartBody.Part.createFormData("remark", "${mSaveData.remark}"))

        mDataBody.add(
            MultipartBody.Part.createFormData(
                "kind_of_goods",
                "${mSaveData.kind_of_goods}"
            )
        )
        mDataBody.add(MultipartBody.Part.createFormData("quantity", "${mSaveData.quantity}"))
        val qRCodeArray =
            FileHelper.createPartQrCode(mSaveData.receip_no) as Array<MultipartBody.Part>
        mDataBody.addAll(qRCodeArray)
        if (mSaveData.userfile.size != 0) {
            val file = FileHelper.createPart(mSaveData.userfile) as Array<MultipartBody.Part>

            apiClient.saveData(file, mDataBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError)
        } else {

            val body: RequestBody = RequestBody.create(MultipartBody.FORM, "")
            val parts = MultipartBody.Part.createFormData("userfile[]", File("").name, body)
            apiClient.saveDataNoImage(parts, mDataBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError)
        }
    }

    fun onSuccess(mDataResponse: SaveDataResponse) {
        mOnSaveDataFinishedListener!!.onSaveSuccess(mDataResponse)
    }

    fun onError(throwable: Throwable) {
        if (throwable is HttpException) {
            if (throwable.code() != 200) {
                mOnSaveDataFinishedListener!!.onSaveError("${throwable.message()}:${throwable.code()}")
            }
            return
        }
        try {
            val json = GsonBuilder()
                .create()
                .toJson(throwable.toString())
            val parser = JsonParser()
            parser.parse(json).asJsonObject
        } catch (er: IllegalStateException) {
            mOnSaveDataFinishedListener!!.onSaveError("Not a JSON")
        }


    }
}