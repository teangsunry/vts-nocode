package vts.app.china.model.response

/**
 * Created By Taing Sunry on 2020-02-03.
 */

data class SaveDataResponse(var save_status: Boolean, var message: String)
