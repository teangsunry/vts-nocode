package vts.app.china.model.request

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.create
import vts.app.china.model.response.RemarkResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.vtsservice.ApiClient
import vts.app.china.vtsservice.ApiInterface
import vts.app.china.vtsview.ReportListInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ReportModel(var context: Context) {


    private var mReportModel: ReportListInterface.reportModel? = null
    private var mPosistion: Int = 0
    fun onUpateStock(
        mReportModel: ReportListInterface.reportModel,
        statusStock: String, id: String,
        position: Int
    ) {
        this.mReportModel = mReportModel
        this.mPosistion = position
        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        apiClient.saveStock(
            clear = "$statusStock",
            id = id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onClearStockSuccess, this::onErrorClearStockModel)

    }

    fun onErrorClearStockModel(throwable: Throwable) {
        mReportModel?.onErrorClearStockModel(throwable)
    }
    fun onClearStockSuccess(responseData: RemarkResponse) {
        mReportModel?.onClearStockSuccess(responseData, mPosistion)
    }

    //load list history
    fun onLoadReportList(
        mReportModel: ReportListInterface.reportModel,
        clear:String,
        ware_house_title: String,
        con_no_id: String,
        search: String,
        page: String,
        isMore: Boolean
    ) {
        this.mReportModel = mReportModel
        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        if (!isMore) {
            apiClient.reportProductList(
                clear=clear,
                ware_house_title = "$ware_house_title",
                con_no_id = "$con_no_id",
                search = "$search",
                page = page
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessModel, this::onErrorModel)
            return
        }

        apiClient.reportProductList(
            clear=clear,
            ware_house_title = "$ware_house_title",
            con_no_id = "$con_no_id",
            search = "$search",
            page = page
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccessModelMore, this::onErrorModel)

    }

    fun onSuccessModelMore(responseData: ReportResponse) {
        mReportModel?.onSuccessModelMore(responseData)
    }

    fun onSuccessModel(responseData: ReportResponse) {
        mReportModel?.onSuccessModel(responseData)
    }

    fun onErrorModel(throwable: Throwable) {
        mReportModel?.onErrorModel(throwable)
    }

}