package vts.app.china.model.response

import java.io.Serializable

/**
 * Created By Taing Sunry on 2020-02-08.
 */

data class ReportResponse(var list_nocode: ArrayList<ReportListData> = arrayListOf()) : Serializable

data class RemarkResponse(var list_nocode: ReportListData) : Serializable
data class ReportListData(
    var id: String = "",
    var receip_no: String = "",
    var code_id:String="",
    var part: String = "",
    var prod_image: String = "",
    var con_id: String = "",
    var con_no_id: String = "",
    var ware_house_title: String = "",
    var loading_date: String = "",
    var created_time: String = "",
    var user: String = "",
    var customer_name: String = "",
    var kind_of_goods: String = "",
    var length: String = "",
    var width: String = "",
    var height: String = "",
    var quantity: String = "",
    var cbm_m3: String = "",
    var weight: String = "",
    var remark: String = "",
    var title: String = "",
    var delivery: String = "",
    var remark_more: String,
    var debit_code: String = "",
    var clear: String = "",
    var remark_no:String=""
    ) : Serializable