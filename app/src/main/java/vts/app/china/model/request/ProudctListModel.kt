package vts.app.china.model.request

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.create
import vts.app.china.model.response.ProductResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.vtsservice.ApiClient
import vts.app.china.vtsservice.ApiInterface
import vts.app.china.vtsview.ProductNoListInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ProudctListModel(var context: Context) : ProductNoListInterface.productModel {

    private var productModel: ProductNoListInterface.productModel? = null

    fun onQueryPrudocts(
        mReportModels: ProductNoListInterface.productModel,
        ware_house_title: String?,
        con_code_id: String?,
        mQueryText: String,
        page: String,
        isMore: Boolean
    ) {
        this.productModel = mReportModels
        val apiClient = ApiClient().getClient(context = context).create<ApiInterface>()
        if (!isMore) {
            apiClient.productList(
                ware_house_title = "$ware_house_title",
                con_no_id = "$con_code_id",
                mQueryText = "$mQueryText",
                page = page
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccessModel, this::onErrorModel)
            return
        }

        apiClient.productList(
            ware_house_title = "$ware_house_title",
            con_no_id = "$con_code_id",
            mQueryText = "$mQueryText",
            page = page
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccessModelMore, this::onErrorModel)

    }

    override fun onSuccessModelMore(responseData: ProductResponse) {
        productModel?.onSuccessModelMore(responseData)
    }

    override fun onSuccessModel(responseData: ProductResponse) {
        productModel?.onSuccessModel(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        productModel?.onErrorModel(throwable)
    }

}