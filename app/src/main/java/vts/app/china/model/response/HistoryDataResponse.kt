package vts.app.china.model.response

import java.io.Serializable

/**
 * Created By Taing Sunry on 2020-02-08.
 */

data class HistoryDataResponse(var dail_report: ArrayList<DataResponse> = arrayListOf()):Serializable

data class DataResponse(var date: String,var entry_number:String):Serializable