package vts.app.china.model.response

data class LoginResponse(
    var message: String = "",
    var login: Boolean = false,
    var userData: UserData
)

data class UserData(
    var id: String = "",
    var username:String="",
    var id_werehouse: String = "",
    var title_warehouse: String = "",
    var id_role: String = "",
    var token: String = "",
    var is_active:String
)
