package vts.app.china.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import vts.app.china.R
import android.graphics.Color
import android.view.View.inflate
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.github.ybq.android.spinkit.style.Wave


object MessageUtits {

    private var messageToast: Toast? = null
    private var messageSnackbar: Snackbar? = null
    private val isShowToast: Boolean = true
    private val isShowSnackbar: Boolean = true

    private var mDialog: AlertDialog.Builder? = null

    fun showProgressbar(context: Context): AlertDialog {

        mDialog = AlertDialog.Builder(context)
        val dialogView = inflate(context, R.layout.loading_view, null)
        mDialog!!.setView(dialogView)
        val progressBar = dialogView!!.findViewById<View>(R.id.spin_kit) as ProgressBar
        val doubleBounce = Wave()
        progressBar.indeterminateDrawable = doubleBounce

        return mDialog!!.create()!!
    }

    /***
     * show message toast
     */
    fun showToast(context: Context,msg: String) {
        messageToast = Toast.makeText(context, "$msg", Toast.LENGTH_SHORT)
        if (isShowToast) {
            messageToast?.show()
        }
    }

    /***
     * show Network connection error
     */
    fun showSnackBarNetworkError(snackbarView: View, context: Context,message: String) {
        messageSnackbar = Snackbar.make(
            snackbarView,
            message,
            Snackbar.LENGTH_LONG
        )
        val sbView = messageSnackbar?.view
        val textView = sbView!!.findViewById(R.id.snackbar_text) as TextView
        textView.setTextColor(Color.YELLOW)
        if (isShowSnackbar) {
            messageSnackbar?.show()
        }
    }

}