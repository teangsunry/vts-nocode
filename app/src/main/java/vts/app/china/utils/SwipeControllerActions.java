package vts.app.china.utils;

/**
 * Created By Taing Sunry on 2020-02-28.
 */

public abstract class SwipeControllerActions {

    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}

}