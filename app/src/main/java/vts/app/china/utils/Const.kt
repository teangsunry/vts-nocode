package vts.app.china.utils

object Const {
//        val BASE_URL = "http://taip.vtsfreight.com/" //server test
    val BASE_URL = "http://caip.vtsfreight.com/" // server live
    val API_KEY = "API_KEY"
    val USER_LOGIN = "USER_LOGIN"
    val API_KEY_USER = "API_KEY_USER"
    val API_KEY_USERP = "API_KEY_USERP"

    val  WHAREHOUSE_TITLE="title_warehouse"

    val SCANNER_REQEUST_CODE = 1020
    val PERMISSION_REQUEST_CODE = 1021
    val REQUEST_IMAGE_CAPTURE = 34964
    val REQUEST_REFRESH_REPORT_CONTAINER = 900
    val WAREHOUSE_CONTAINER_CODE=10112
}
