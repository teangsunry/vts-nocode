package vts.app.china.utils

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.DialogFragment
import com.github.ybq.android.spinkit.style.Wave
import vts.app.china.R


/**
 * Created By Taing Sunry on 2020-02-08.
 */

class LoadingDialogFragment : DialogFragment() {

    fun newInstance(): LoadingDialogFragment {
        val f = LoadingDialogFragment()
        return f
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.loading_view, container, false)
    }

    override fun onViewCreated(dialogView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(dialogView, savedInstanceState)
        val progressBar = dialogView!!.findViewById<View>(R.id.spin_kit) as ProgressBar
        val doubleBounce = Wave()
        progressBar.indeterminateDrawable = doubleBounce
    }

}