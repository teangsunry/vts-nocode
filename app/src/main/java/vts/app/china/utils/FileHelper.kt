package vts.app.china.utils

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import vts.app.china.database.PhotoEntity
import java.io.File

/**
 * Created By Taing Sunry on 2020-02-04.
 */

object FileHelper {

    fun createFile(realPath: String): File {
        return File(realPath)
    }

    fun createPartFromString(text: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), text)
    }

    fun createPart(file: ArrayList<PhotoEntity>): Array<MultipartBody.Part?> {
        val parts = arrayOfNulls<MultipartBody.Part>(file.size)
        val MEDIA_TYPE_IMAGE: MediaType = MediaType.parse("image/*")!!
        for (i in 0 until file.size) {
            val imageFile = File(file[i].photo_url)
            val body: RequestBody = RequestBody.create(MEDIA_TYPE_IMAGE, imageFile)
            println("fileName:${imageFile.name}")

            parts[i] = MultipartBody.Part.createFormData("userfile[]", imageFile.name, body)
            println("urlFile:${imageFile}")
        }
        return parts
    }

    fun createPartQrCode(remarkArray: ArrayList<String>): Array<MultipartBody.Part?> {
        val partsRemark = arrayOfNulls<MultipartBody.Part>(remarkArray.size)
        for (i in remarkArray.indices) {
            partsRemark[i] = MultipartBody.Part.createFormData("receip_no[]", "${remarkArray[i]}")
        }
        return partsRemark
    }

}