package vts.app.china.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import vts.app.china.R
import vts.app.china.model.response.UserData
import com.google.gson.reflect.TypeToken
import android.util.Log

object PrefUtils {

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(
            context.getString(vts.app.china.R.string.app_name),
            Context.MODE_PRIVATE
        )
    }

    fun saveApiKey(context: Context, apiKey: String) {
        val editor = getSharedPreferences(context).edit()
        editor.putString(Const.API_KEY, apiKey)
        editor.commit()
    }

    fun getApiKey(context: Context): String? {
        return getSharedPreferences(context).getString(Const.API_KEY, null)
    }

    fun saveUser(context: Context, userU: String, userP: String) {
        val editor = getSharedPreferences(context).edit()
        editor.putString(Const.API_KEY_USER, AESHelper.encrypt(userU))
        editor.putString(Const.API_KEY_USERP, AESHelper.encrypt(userP))
        editor.commit()
    }

    fun getUserPKey(context: Context): String? {
        return if (!getSharedPreferences(context).getString(
                Const.API_KEY_USERP,
                ""
            ).isNullOrBlank()
        ) {
            AESHelper.decrypt(getSharedPreferences(context).getString(Const.API_KEY_USERP, ""))
        } else {
            ""
        }
    }

    fun getUserUKey(context: Context): String? {
        return if (!getSharedPreferences(context).getString(
                Const.API_KEY_USERP,
                ""
            ).isNullOrBlank()
        ) {
            AESHelper.decrypt(getSharedPreferences(context).getString(Const.API_KEY_USER, ""))
        } else {
            ""
        }
    }

    fun isLoginIcon(context: Context): Int {
        return if (getApiKey(context) != null) R.drawable.ic_sign_out else R.drawable.ic_lock_outline_black_24dp
    }

    fun isLoginText(context: Context): Int {
        return if (getApiKey(context) != null) R.string.logout else R.string.lbllogin
    }

    fun saveLoginKey(context: Context, mUserData: UserData) {
        val editor = getSharedPreferences(context).edit()
        val gson = Gson()
        val json = gson.toJson(mUserData)
        editor.putString(Const.USER_LOGIN, json).apply()
    }

    fun getLoginData(context: Context): UserData? {
        val userDataJson = getSharedPreferences(context).getString(Const.USER_LOGIN, "{}")
        val gsonBuilder = Gson()
        val data = gsonBuilder.fromJson<UserData>(userDataJson, UserData::class.java)
        return data
    }

    fun signOutUser(context: Context) {
        context.getSharedPreferences(
            context.getString(vts.app.china.R.string.app_name),
            Context.MODE_PRIVATE
        ).edit().clear().apply()
    }

    fun putIntegerArrayList(key: String, list: ArrayList<String>?, context: Context) {
        val editor = context.getSharedPreferences(
            "search",
            Context.MODE_PRIVATE
        ).edit()

        val mGetArrayData = getIntegerArrayList(key, list, context)
        if (mGetArrayData?.size!! >= 5) {
            list?.removeAt(0)
        }
        editor.putString(key, list?.joinToString(",") ?: "").apply()

    }

    fun getIntegerArrayList(
        key: String,
        defValue: ArrayList<String>?,
        context: Context
    ): ArrayList<String>? {
        val mPreferences = context.getSharedPreferences(
            "search",
            Context.MODE_PRIVATE
        )
        val value = mPreferences.getString(key, null)
        println(value)
        if (value.isNullOrBlank())
            return defValue
        return ArrayList(value.split(",").map { it })
    }


    fun putQrCodeArrayList(key: String, list: ArrayList<String>?, context: Context) {
        val editor = context.getSharedPreferences(
            "qrCodeList",
            Context.MODE_PRIVATE
        ).edit()

        val mGetArrayData = getQrCodeArrayList(key, list, context)
        if (mGetArrayData?.size!! >= 3) {
            list?.removeAt(0)
        }
        editor.putString(key, list?.joinToString(",") ?: "").apply()

    }

    fun getQrCodeArrayList(
        key: String,
        defValue: ArrayList<String>?,
        context: Context
    ): ArrayList<String>? {
        val mPreferences = context.getSharedPreferences(
            "qrCodeList",
            Context.MODE_PRIVATE
        )
        val value = mPreferences.getString(key, null)
        println(value)
        if (value.isNullOrBlank())
            return defValue
        return ArrayList(value.split(",").map { it })
    }

}