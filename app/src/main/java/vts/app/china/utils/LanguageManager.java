package vts.app.china.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class LanguageManager {

    private static LanguageManager mLanguagePreferenceManager;
    private SharedPreferences sharedPreferences;

    public static LanguageManager getInstance(Context context) {
        if (mLanguagePreferenceManager == null) {
            mLanguagePreferenceManager = new LanguageManager(context);
        }
        return mLanguagePreferenceManager;
    }

    private LanguageManager(Context context) {
        sharedPreferences = context.getSharedPreferences("CHANGE_LANGUAGES", Context.MODE_PRIVATE);
    }

    public void saveLanguage(String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("LANGUAGE", value);
        prefsEditor.commit();
    }

    public String getLanguage() {
        return sharedPreferences.getString("LANGUAGE", "en");
    }


}
