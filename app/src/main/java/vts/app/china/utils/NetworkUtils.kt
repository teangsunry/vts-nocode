package vts.app.china.utils

import android.content.Context
import android.net.ConnectivityManager

object NetworkUtils {
    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        @Suppress("DEPRECATION") val networkInfo = connectivityManager.activeNetworkInfo
        return !(networkInfo == null || !networkInfo.isConnected)
    }
}