package vts.app.china.utils;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created By Taing Sunry on 2020-01-31.
 */

abstract class CustomeTextChange implements TextWatcher {

    public abstract void MybeforeTextChanged(CharSequence s, int start, int count, int after);

    public abstract void MyonTextChanged(CharSequence s, int start, int before, int count);

    public abstract void MyafterTextChanged(Editable s);

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        MybeforeTextChanged(s, start, count, after);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        MyonTextChanged(s, start, before, count);
    }

    @Override
    public void afterTextChanged(Editable s) {
        MyafterTextChanged(s);
    }
}
