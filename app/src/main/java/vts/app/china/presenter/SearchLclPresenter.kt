package vts.app.china.presenter

import android.content.Context
import vts.app.china.model.postbody.SaveDataOrdering
import vts.app.china.model.request.SaveDataReqestModel
import vts.app.china.model.response.SaveDataResponse
import vts.app.china.vtsview.SearchInterface

/**
 * Created By Taing Sunry on 2020-01-30.
 */

class SearchLclPresenter(var mSearchLclView: SearchInterface.SearchLclView, var context: Context) :
    SearchInterface.presenter,SaveDataReqestModel.OnSaveDataFinishedListener {
    override fun onSaveSuccess(data: SaveDataResponse) {
        mSearchLclView?.hideProgressing()
        mSearchLclView?.onSaveSuccessView(data)
    }

    override fun onSaveError(throwable: Any) {
        mSearchLclView?.onSaveError(throwable)
        mSearchLclView?.hideProgressing()

    }

    override fun onStartSaveData(mSaveDataOrdering: SaveDataOrdering) {
        mSearchLclView?.showProgressing()
        if (!mSaveDataOrdering.con_no_id.isNullOrBlank()) {
            val mSaveDataReqestModel = SaveDataReqestModel(context)
            mSaveDataReqestModel.onSaveData(mSaveDataOrdering, this)
            return
        }
        onSaveError("No id is empty!.")
    }


}