package vts.app.china.presenter

import android.content.Context
import vts.app.china.model.request.ProudctListModel
import vts.app.china.model.request.ReportModel
import vts.app.china.model.response.ProductResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.vtsview.ProductNoListInterface
import vts.app.china.vtsview.ReportListInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ProductListPresenter(
    var productView: ProductNoListInterface.productView,
    var context: Context
) :
    ProductNoListInterface.productModel, ProductNoListInterface.productNoPresenter {


    override fun onQueryProductList(
        ware_house_title: String?,
        con_code_id: String?,
        mQueryText: String,
        page: String,
        isMore: Boolean
    ) {
        val mProudctListModel = ProudctListModel(context)
        mProudctListModel.onQueryPrudocts(
            this,
            ware_house_title = ware_house_title,
            con_code_id = con_code_id,
            mQueryText = mQueryText,
            page = page,
            isMore = isMore
        )
    }

    override fun onSuccessModelMore(responseData: ProductResponse) {
        productView?.onSuccessModelMore(responseData)
    }

    override fun onSuccessModel(responseData: ProductResponse) {
        productView.onSuccess(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        productView.onError(throwable)
    }
}