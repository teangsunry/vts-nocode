package vts.app.china.presenter

import android.content.Context
import vts.app.china.model.request.RemarkModel
import vts.app.china.model.request.WareHouseModel
import vts.app.china.model.response.RemarkResponse
import vts.app.china.model.response.ReportListData
import vts.app.china.model.response.WareHouseData
import vts.app.china.vtsview.RemarkInterface
import vts.app.china.vtsview.WareHouseInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class RemakrPresenter(var remarkView: RemarkInterface.remarkView, var context: Context) :

    RemarkInterface.remarkPresenter, RemarkInterface.remarkModel {
    override fun onSaveRemark(remark:String,id:String) {
        val mSaveRemark = RemarkModel(context)
        mSaveRemark.onAddRemark(this,remark,id)
    }


    override fun onSuccessModel(responseData: RemarkResponse) {
        remarkView?.onSuccess(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        remarkView?.onError(throwable)
    }



}