package vts.app.china.presenter

import android.content.Context
import vts.app.china.model.request.ReportModel
import vts.app.china.model.response.RemarkResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.vtsview.ReportListInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ReportListPresenter(var reportView: ReportListInterface.reportView, var context: Context) :
    ReportListInterface.reportModel, ReportListInterface.reportPresent {
    override fun onErrorClearStockModel(throwable: Throwable) {
        reportView?.onErrorClearStockModel(throwable)
    }

    override fun clearStock(status: String, id: String, position: Int) {
        val mReportRequest = ReportModel(context)
        mReportRequest.onUpateStock(
            this,
            statusStock = status,
            id = id,
            position = position
        )
    }

    override fun onClearStockSuccess(responseData: RemarkResponse, position: Int) {
        reportView?.onClearStockSuccess(responseData, position)
    }


    override fun onLoadProductListReport(
        clear:String,
        ware_house_title: String,
        con_no_id: String,
        search: String,
        page: String,
        isMore: Boolean
    ) {
        val mReportRequest = ReportModel(context)
        mReportRequest.onLoadReportList(
            this,
            clear = clear,
            ware_house_title = ware_house_title,
            con_no_id = con_no_id,
            search = search,
            page = page,
            isMore = isMore
        )
    }

    override fun onSuccessModelMore(responseData: ReportResponse) {
        reportView?.onSuccessModelMore(responseData)
    }

    override fun onSuccessModel(responseData: ReportResponse) {
        reportView.onSuccess(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        reportView.onError(throwable)
    }


}