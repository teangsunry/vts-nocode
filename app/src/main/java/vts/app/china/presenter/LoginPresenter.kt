package vts.app.china.presenter

import android.content.Context
import vts.app.china.model.request.LoginRequestModel
import vts.app.china.model.response.LoginResponse
import vts.app.china.vtsview.LoginInterface

/**
 * Created By Taing Sunry on 2020-01-30.
 */

class LoginPresenter(var mLoginView: LoginInterface.LoginView, var context: Context) :
    LoginInterface.presenter, LoginRequestModel.OnLoginFinishedListener {

    override fun onError(throwable: Any) {
        mLoginView.hideProgressing()
        mLoginView.onError(throwable)
    }

    override fun onStartReqest(type: String, keySearch: String) {
        mLoginView.showProgressing()
        LoginRequestModel(context).reqestLogin(this, type, keySearch)
    }

    override fun onSuccess(data: LoginResponse) {
        mLoginView.hideProgressing()
        mLoginView.onSuccessView(data)
    }

}