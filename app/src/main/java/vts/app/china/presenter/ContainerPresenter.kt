package vts.app.china.presenter

import android.content.Context
import vts.app.china.model.request.ContainerModel
import vts.app.china.model.request.ReportModel
import vts.app.china.model.response.ContainerResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.vtsview.ContainerInterface
import vts.app.china.vtsview.ReportListInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ContainerPresenter(var reportView: ContainerInterface.containerView, var context: Context,var isReport:Boolean=false) :
    ContainerInterface.containerModel, ContainerInterface.containerPresent {

    override fun onLoadContainerList(ware_house_title: String, page: String, isMore: Boolean) {
        val mContainerModel = ContainerModel(context)
        if (!isReport){
            mContainerModel.onLoadContainerList(
                this,
                ware_house_title = ware_house_title,
                page = page,
                isMore = isMore
            )
            return
        }
        mContainerModel.onLoadContainerReportList(
            this,
            ware_house_title = ware_house_title,
            page = page,
            isMore = isMore
        )

    }


    override fun onSuccessModelMore(responseData: ContainerResponse) {
        reportView?.onSuccessModelMore(responseData)
    }

    override fun onSuccessModel(responseData: ContainerResponse) {
        reportView.onSuccess(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        reportView.onError(throwable)
    }


}