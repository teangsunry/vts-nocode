package vts.app.china.presenter

import android.content.Context
import vts.app.china.model.request.WareHouseModel
import vts.app.china.model.response.WareHouseData
import vts.app.china.vtsview.WareHouseInterface

/**
 * Created By Taing Sunry on 2020-02-08.
 */

class WareHousePresenter(var warehouseView: WareHouseInterface.warehouseView, var context: Context) :

    WareHouseInterface.warehousePresenter, WareHouseInterface.warehouseModel {


    override fun onSuccessModel(responseData: WareHouseData) {
        warehouseView?.onSuccess(responseData)
    }

    override fun onErrorModel(throwable: Throwable) {
        warehouseView?.onError(throwable)
    }

    override fun onLoadData() {
        val mWareHouseModel = WareHouseModel(context)
        mWareHouseModel.onRequestWarehouseData(this)
    }

    override fun LoadReport(clear:String) {
        val mWareHouseModel = WareHouseModel(context)
        mWareHouseModel.onLoadWarehouseReport(this,clear)
    }
}