package vts.app.china.vtsview
import vts.app.china.model.response.WareHouseData
import vts.app.china.presenter.BasePresenter

/**
 * Created By Taing Sunry on 2020-02-08.
 */

interface WareHouseInterface {
    interface warehousePresenter : BasePresenter {
        fun onLoadData()
        fun LoadReport(clear:String="")
    }

    interface warehouseView : BaseView<warehousePresenter> {
        fun onSuccess(responseData: WareHouseData)
        fun onError(throwable: Throwable)
    }

    interface warehouseModel{
        fun onSuccessModel(responseData: WareHouseData)
        fun onErrorModel(throwable: Throwable)
    }
}