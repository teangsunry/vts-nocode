package vts.app.china.vtsview
import vts.app.china.model.response.ContainerResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.model.response.WareHouseData
import vts.app.china.presenter.BasePresenter

/**
 * Created By Taing Sunry on 2020-02-08.
 */

interface ContainerInterface {
    interface containerPresent : BasePresenter {
        fun onLoadContainerList(
            ware_house_title: String,
            page: String,
            isMore: Boolean
        )
    }

    interface containerView : BaseView<containerPresent> {
        fun onSuccess(responseData: ContainerResponse)
        fun onSuccessModelMore(responseData: ContainerResponse)
        fun onError(throwable: Throwable)
    }

    interface containerModel {
        fun onSuccessModel(responseData: ContainerResponse)
        fun onSuccessModelMore(responseData: ContainerResponse)
        fun onErrorModel(throwable: Throwable)
    }
}