package vts.app.china.vtsview

import vts.app.china.model.response.RemarkResponse
import vts.app.china.model.response.ReportResponse
import vts.app.china.presenter.BasePresenter

/**
 * Created By Taing Sunry on 2020-02-08.
 */

interface ReportListInterface {
    interface reportPresent : BasePresenter {
        fun onLoadProductListReport(
            clear: String,
            ware_house_title: String,
            con_no_id: String,
            search: String,
            page: String,
            isMore: Boolean
        )

        fun clearStock(
            status: String,
            id: String,
            position: Int
        )
    }

    interface reportView : BaseView<reportPresent> {
        fun onSuccess(responseData: ReportResponse)
        fun onSuccessModelMore(responseData: ReportResponse)
        fun onError(throwable: Throwable)
        fun onErrorClearStockModel(throwable: Throwable)
        fun onClearStockSuccess(responseData: RemarkResponse, position: Int)
    }

    interface reportModel {
        fun onSuccessModel(responseData: ReportResponse)
        fun onSuccessModelMore(responseData: ReportResponse)
        fun onErrorModel(throwable: Throwable)
        fun onErrorClearStockModel(throwable: Throwable)
        fun onClearStockSuccess(responseData: RemarkResponse, position: Int)
    }
}