package vts.app.china.vtsview


interface BaseView<T> {
    fun setPresenter(presenter : T)
    fun onError(t:Any)
    fun onErrorConnection(any: Any)
}
