package vts.app.china.vtsview
import vts.app.china.model.response.RemarkResponse
import vts.app.china.model.response.ReportListData
import vts.app.china.model.response.WareHouseData
import vts.app.china.presenter.BasePresenter

/**
 * Created By Taing Sunry on 2020-02-08.
 */

interface RemarkInterface {
    interface remarkPresenter : BasePresenter {
        fun onSaveRemark(remark:String,id:String)
    }

    interface remarkView : BaseView<remarkPresenter> {
        fun onSuccess(responseData: RemarkResponse)
        fun onError(throwable: Throwable)
    }

    interface remarkModel{
        fun onSuccessModel(responseData: RemarkResponse)
        fun onErrorModel(throwable: Throwable)
    }
}