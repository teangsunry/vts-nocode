package vts.app.china.vtsview

import vts.app.china.model.response.ProductResponse
import vts.app.china.presenter.BasePresenter

/**
 * Created By Taing Sunry on 2020-02-08.
 */

interface ProductNoListInterface {
    interface productNoPresenter : BasePresenter {
        fun onQueryProductList(
            title_warehouse: String?,
            con_code_id:String?,
            mQueryText: String,
            page: String,
            isMore: Boolean
        )
    }

    interface productView : BaseView<productNoPresenter> {
        fun onSuccess(responseData: ProductResponse)
        fun onSuccessModelMore(responseData: ProductResponse)
        fun onError(throwable: Throwable)
    }

    interface productModel {
        fun onSuccessModel(responseData: ProductResponse)
        fun onSuccessModelMore(responseData: ProductResponse)
        fun onErrorModel(throwable: Throwable)
    }
}