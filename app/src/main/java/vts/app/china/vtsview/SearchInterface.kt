package vts.app.china.vtsview

import vts.app.china.model.postbody.SaveDataOrdering
import vts.app.china.model.response.SaveDataResponse
import vts.app.china.presenter.BasePresenter

interface SearchInterface {

    interface presenter : BasePresenter {
        fun onStartSaveData(mSaveDataOrdering: SaveDataOrdering)
    }

    interface SearchLclView : BaseView<presenter> {
        fun showProgressing()
        fun hideProgressing()
        fun onSaveSuccessView(data: SaveDataResponse)
        fun onSaveError(throwable: Any)
    }

}

