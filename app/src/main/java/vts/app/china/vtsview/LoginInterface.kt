package vts.app.china.vtsview

import vts.app.china.model.response.LoginResponse
import vts.app.china.presenter.BasePresenter

interface LoginInterface {

    interface presenter : BasePresenter {
        fun onStartReqest(type: String, keySearch: String)
    }

    interface LoginView : BaseView<presenter> {
        fun showProgressing()
        fun hideProgressing()
        fun onSuccessView(data: LoginResponse)
    }

}