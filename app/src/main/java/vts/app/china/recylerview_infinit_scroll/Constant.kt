package vts.app.china.recylerview_infinit_scroll

/**
 * Created By Taing Sunry on 2020-02-15.
 */

object Constant {
    const val VIEW_TYPE_ITEM = 0
    const val VIEW_TYPE_LOADING = 1
}