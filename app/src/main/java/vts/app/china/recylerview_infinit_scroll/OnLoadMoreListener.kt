package vts.app.china.recylerview_infinit_scroll

/**
 * Created By Taing Sunry on 2020-02-15.
 */

interface OnLoadMoreListener {
    fun onLoadMore()
}