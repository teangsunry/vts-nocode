package vts.app.china.vtsui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ComplexColorCompat.inflate
import com.github.ybq.android.spinkit.style.Wave
import kotlinx.android.synthetic.main.activity_login.*
import vts.app.china.R
import vts.app.china.model.response.LoginResponse
import vts.app.china.presenter.LoginPresenter
import vts.app.china.utils.MessageUtits
import vts.app.china.utils.NetworkUtils
import vts.app.china.utils.PrefUtils
import vts.app.china.vtsview.LoginInterface

/**
 * Created By Taing Sunry on 2020-01-29.
 */

class LoginActivity : BaseActivity(), LoginInterface.LoginView {

    private var mLoginPresenter: LoginInterface.presenter? = null

    private var mDialog: AlertDialog? = null
    private var userName: String = ""
    private var passWord: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setPresenter(LoginPresenter(this, this))
        loginView()
    }

    private fun loginView() {
        user_name_edit_text?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                user_name_text_input?.isErrorEnabled = false
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        password_edit_text?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                password_text_input?.isErrorEnabled = false
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
        btnLogin?.setOnClickListener {
            // define get text from input
            userName = user_name_edit_text!!.text?.toString().toString()
            passWord = password_edit_text!!.text?.toString().toString()

            // validation connection is online
            if (!NetworkUtils.isOnline(this)) {
                MessageUtits.showSnackBarNetworkError(
                    loginCoordinator,
                    this,
                    getString(R.string.connection_error)
                )
                return@setOnClickListener
            }

            // validation username is empty input
            if (userName?.isNullOrBlank()!!) {
                user_name_text_input?.error = getString(R.string.input_error_username_empty)
                return@setOnClickListener
            }

            // validation password empty input
            if (passWord?.isNullOrBlank()!!) {
                user_name_text_input?.isErrorEnabled = false
                password_text_input?.error = getString(R.string.input_error_password_empty)
                return@setOnClickListener
            }
            mLoginPresenter?.onStartReqest(userName, passWord)
        }
    }

    override fun setPresenter(presenter: LoginInterface.presenter) {
        mLoginPresenter = presenter
    }

    override fun showProgressing() {
        mDialog = MessageUtits.showProgressbar(this)
        mDialog!!.show()
    }

    override fun hideProgressing() {
        if (!mDialog!!.isShowing && mDialog != null) {
            mDialog!!.dismiss()
            mDialog = null
        }
    }

    override fun onErrorConnection(messageData: Any) {
        tvMessage?.visibility = View.VISIBLE
        tvMessage?.text = messageData.toString()
        MessageUtits.showToast(this, "$messageData")
    }

    override fun onError(t: Any) {
        MessageUtits.showToast(this, "${t}")
    }

    override fun onSuccessView(data: LoginResponse) {
        mDialog!!.dismiss()
        if (data.login==false){
            Toast.makeText(this, "${data.message}", Toast.LENGTH_SHORT).show()
            mDialog = null
            return
        }
        if (!mDialog!!.isShowing && mDialog != null) {
            PrefUtils.saveApiKey(
                this,
                "${if (data.userData != null) data.userData.token else ""}"
            )
            if (!data.userData.is_active.isNullOrBlank() && data.userData.is_active == "1") {
                PrefUtils.saveLoginKey(this, data.userData)
                PrefUtils.saveUser(this, "$userName", "$passWord")
                val homeActivity = Intent(this, MainActivity::class.java)
                homeActivity.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(homeActivity)
                this.finish()
            } else {
                if (PrefUtils.getLoginData(this) != null) {
                    PrefUtils.signOutUser(this)
                    Toast.makeText(this, "User inactive", Toast.LENGTH_SHORT).show()
                }
            }

            mDialog = null
        }

    }


}