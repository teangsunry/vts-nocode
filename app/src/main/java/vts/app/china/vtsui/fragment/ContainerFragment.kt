package vts.app.china.vtsui.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_container.*
import vts.app.china.R
import vts.app.china.model.response.ContainerData
import vts.app.china.model.response.ContainerResponse
import vts.app.china.presenter.ContainerPresenter
import vts.app.china.recylerview_infinit_scroll.OnLoadMoreListener
import vts.app.china.recylerview_infinit_scroll.RecyclerViewLoadMoreScroll
import vts.app.china.vtsui.adpater.ContainerListAdapter
import vts.app.china.vtsview.ContainerInterface
import vts.app.china.utils.Const.WHAREHOUSE_TITLE
import vts.app.china.utils.NetworkUtils
import vts.app.china.vtsui.ProductNoListActivity


/**
 * Created By Taing Sunry on 2020-02-21.
 */

class ContainerFragment : Fragment(), ContainerListAdapter.itemListenter,
    ContainerInterface.containerView {

    private var mContext: Activity? = null

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var mPresenter: ContainerInterface.containerPresent
    private lateinit var viewAdapter: ContainerListAdapter
    private lateinit var scrollListener: RecyclerViewLoadMoreScroll
    private var mDataList = arrayListOf<ContainerData?>()

    private var title_warehouse: String = ""
    private var page: Int = 1
    private var positionAdapter: Int = 0

    private fun requestData(
        title_warehouse: String,
        page: String,
        isMore: Boolean
    ) {
        if (!isMore) {
            this.page = 1
            mSwepRefresh?.isRefreshing = true
        }
        mPresenter.onLoadContainerList(
            ware_house_title = title_warehouse,
            page = page,
            isMore = isMore
        )
    }

    companion object {
        fun newInstance(param1: String): ContainerFragment {
            val fragment = ContainerFragment()
            val args = Bundle()
            args.putString(WHAREHOUSE_TITLE, param1)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        positionAdapter = 0
        mContext = activity
        setPresenter(ContainerPresenter(this, mContext!!))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_container, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        title_warehouse = "${arguments?.getString(WHAREHOUSE_TITLE, "")}"

        setUpList(view)

        requestData(
            title_warehouse = title_warehouse,
            page = "$page",
            isMore = false
        )
        mSwepRefresh?.setOnRefreshListener {
            page = 1
            requestData(
                title_warehouse = title_warehouse,
                page = "$page",
                isMore = false
            )
        }
    }

    private fun setUpList(view: View) {
        viewManager = LinearLayoutManager(activity!!)
        viewAdapter = ContainerListAdapter(mDataList!!, this, activity!!)
        recyclerView = view.findViewById<RecyclerView>(R.id.mRecylerViewWarehouse).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        setRVScrollListener()
    }

    private fun setRVScrollListener() {
        scrollListener = RecyclerViewLoadMoreScroll(viewManager as LinearLayoutManager)
        scrollListener.setOnLoadMoreListener(object :
            OnLoadMoreListener {
            override fun onLoadMore() {
                viewAdapter?.addLoadingView()
                requestData(
                    title_warehouse = title_warehouse,
                    page = "$page",
                    isMore = true
                )

            }
        })
        recyclerView.addOnScrollListener(scrollListener)
    }


    override fun onIntent(data: ContainerData, position: Int) {
        if (!NetworkUtils.isOnline(mContext!!)) {
            Toast.makeText(mContext, "No internet connection!", Toast.LENGTH_SHORT).show()
            return
        }
        positionAdapter = position
        val mIntent = Intent(mContext, ProductNoListActivity::class.java)
        mIntent.putExtra("data", data)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivityForResult(mIntent, 123)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 123 && resultCode === Activity.RESULT_OK) {
            page = 1
            requestData(title_warehouse = title_warehouse, page = "$page", isMore = false)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSuccess(responseData: ContainerResponse) {
        mDataList?.clear()
        mDataList = responseData!!.get_list as ArrayList<ContainerData?>
        viewAdapter?.notifyDataSetChanged()
        viewAdapter?.updateData(mDataList)

        mSwepRefresh?.isRefreshing = false
        var view = if (mDataList?.size == 0) View.VISIBLE else View.GONE
        tvNoData?.visibility = view
        page++
        scrollListener?.setLoaded()
    }

    override fun onSuccessModelMore(responseData: ContainerResponse) {
        viewAdapter?.removeLoadingView()
        mDataList = responseData!!.get_list as ArrayList<ContainerData?>
        mSwepRefresh?.isRefreshing = false
        if (mDataList?.size == 0) {
            return
        }
        page++
        viewAdapter?.addData(mDataList)
        scrollListener?.setLoaded()
    }

    override fun onError(throwable: Throwable) {
        mSwepRefresh?.isRefreshing = false
    }

    override fun setPresenter(presenter: ContainerInterface.containerPresent) {
        mPresenter = presenter
    }

    override fun onError(t: Any) {
        mSwepRefresh?.isRefreshing = false
    }

    override fun onErrorConnection(any: Any) {
        mSwepRefresh?.isRefreshing = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.action_search_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item?.itemId == R.id.action_searcch) {
            onIntent(ContainerData(ware_house_title = title_warehouse), 0)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}