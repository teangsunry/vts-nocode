package vts.app.china.vtsui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import vts.app.china.R
import android.content.Intent
import kotlinx.android.synthetic.main.activity_revise_goods.*
import android.view.View
import vts.app.china.recylerview_infinit_scroll.OnLoadMoreListener
import vts.app.china.recylerview_infinit_scroll.RecyclerViewLoadMoreScroll
import vts.app.china.model.response.*
import vts.app.china.presenter.ProductListPresenter
import vts.app.china.vtsui.adpater.ProductListAdapter
import vts.app.china.vtsview.ProductNoListInterface
import android.app.Activity
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import vts.app.china.utils.Const.WAREHOUSE_CONTAINER_CODE
import vts.app.china.utils.NetworkUtils


/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ProductNoListActivity : BaseActivity(), ProductListAdapter.itemListenter,
    ProductNoListInterface.productView {

    private var mDataList = arrayListOf<ProductData?>()
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var productsPresenter: ProductNoListInterface.productNoPresenter
    private var viewAdapter: ProductListAdapter? = null
    private var scrollListener: RecyclerViewLoadMoreScroll? = null

    private var title_warehouse: String = ""
    private var con_code_id: String = ""
    private var page: Int = 1
    private var positionAdapter: Int = 0
    private var mQueryText: String = ""
    private var mDataIntent: ContainerData? = null
    private var isResultContainer = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isResultContainer = Activity.RESULT_CANCELED

        setContentView(R.layout.activity_product_no_list)
        setPresenter(ProductListPresenter(this, this))
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        mDataIntent = intent?.getSerializableExtra("data") as ContainerData
        title_warehouse = mDataIntent?.ware_house_title ?: ""
        con_code_id = "${mDataIntent?.con_no_id}"

        supportActionBar?.title = title_warehouse
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        if (!NetworkUtils.isOnline(this)) {
            Toast.makeText(this, "No Internet connection!.", Toast.LENGTH_SHORT).show()
        } else {
            requestData(
                title_warehouse = title_warehouse,
                con_code_id = con_code_id,
                search = "$mQueryText",
                page = "$page",
                isMore = false
            )
        }
        swipeRefresh?.setOnRefreshListener {
            if (!NetworkUtils.isOnline(this)) {
                swipeRefresh?.isRefreshing = false
                Toast.makeText(this, "No Internet connection!.", Toast.LENGTH_SHORT).show()
                return@setOnRefreshListener
            }
            page = 1
            requestData(
                title_warehouse = title_warehouse,
                con_code_id = con_code_id,
                search = "$mQueryText",
                page = "$page",
                isMore = false
            )
        }

    }

    private fun requestData(
        title_warehouse: String,
        con_code_id: String,
        search: String,
        page: String,
        isMore: Boolean
    ) {
        if (!isMore) {
            this.page = 1
            mQueryText = ""
            swipeRefresh?.isRefreshing = true
        }
        productsPresenter.onQueryProductList(
            title_warehouse = title_warehouse,
            con_code_id = con_code_id,
            mQueryText = search,
            page = page,
            isMore = isMore
        )
    }

    override fun onError(throwable: Throwable) {
        tvNoDataFound?.visibility = View.VISIBLE
        swipeRefresh?.isRefreshing = false
    }

    override fun setPresenter(presenter: ProductNoListInterface.productNoPresenter) {
        productsPresenter = presenter
    }

    override fun onError(t: Any) {
        swipeRefresh?.isRefreshing = false
    }

    override fun onErrorConnection(any: Any) {
        swipeRefresh?.isRefreshing = false
    }

    override fun onIntent(data: ProductData, position: Int) {
        positionAdapter = position
        val intentDetail = Intent(this, NoCodeActivity::class.java)
        intentDetail.putExtra("data", data)
        intentDetail.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivityForResult(intentDetail, WAREHOUSE_CONTAINER_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == WAREHOUSE_CONTAINER_CODE && resultCode === Activity.RESULT_OK) {
            if (viewAdapter != null) {
                isResultContainer = Activity.RESULT_OK
                viewAdapter?.removeItems(positionAdapter)
                if (viewAdapter?.itemCount == 0) {
                    setUpList()
                    tvNoDataFound?.visibility = View.VISIBLE
                    swipeRefresh?.isRefreshing = false
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setUpList() {
        viewAdapter = null
        viewManager = LinearLayoutManager(this)
        viewAdapter = ProductListAdapter(mDataList, this, this)
        recyclerView = findViewById<RecyclerView>(R.id.mRecylerListNo)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = viewManager
        recyclerView?.adapter = viewAdapter
        setRVScrollListener()
    }

    private fun setRVScrollListener() {
        scrollListener?.let {
            recyclerView?.removeOnScrollListener(it)
        }

        scrollListener = RecyclerViewLoadMoreScroll(viewManager as LinearLayoutManager)
        scrollListener?.setOnLoadMoreListener(object :
            OnLoadMoreListener {
            override fun onLoadMore() {
                viewAdapter?.addLoadingView()
                requestData(
                    title_warehouse = title_warehouse,
                    con_code_id = con_code_id,
                    search = "$mQueryText",
                    page = "$page",
                    isMore = true
                )

            }
        })
        recyclerView.addOnScrollListener(scrollListener!!)
    }

    override fun onSuccess(responseData: ProductResponse) {

        var view = if (responseData?.get_list?.size == 0) View.VISIBLE else View.GONE
        setUpList()
        viewAdapter?.addData(responseData.get_list as ArrayList<ProductData?>)
        swipeRefresh?.isRefreshing = false
        tvNoDataFound?.visibility = view
        page++
    }

    override fun onSuccessModelMore(responseData: ProductResponse) {
        viewAdapter?.removeLoadingView()
        swipeRefresh?.isRefreshing = false
        if (responseData?.get_list?.size == 0) {
            return
        }
        mDataList = responseData!!.get_list as ArrayList<ProductData?>
        page++
        viewAdapter?.addDataMore(mDataList)
        scrollListener?.setLoaded()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchViewItem = menu?.findItem(R.id.action_master_search)
        val searchViewAndroidActionBar = MenuItemCompat.getActionView(searchViewItem) as SearchView

        if (con_code_id.isNullOrBlank()) {
            searchViewItem?.expandActionView()
        }

        searchViewAndroidActionBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchViewAndroidActionBar?.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mQueryText = "$newText"
                page = 1
                requestData(
                    title_warehouse = title_warehouse,
                    con_code_id = con_code_id,
                    search = "$mQueryText",
                    page = "$page",
                    isMore = false
                )
                return true
            }

        })
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(isResultContainer)
        super.onBackPressed()
    }

}