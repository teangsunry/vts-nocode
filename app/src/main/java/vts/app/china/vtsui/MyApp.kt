package vts.app.china.vtsui

import android.app.Application
import android.content.Context
import vts.app.china.utils.LocaleHelper


/**
 * Created By Taing Sunry on 2020-01-29.
 */

class MyApp : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"))
    }

}