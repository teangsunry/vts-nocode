package vts.app.china.vtsui

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import vts.app.china.R
import vts.app.china.model.response.LoginResponse
import vts.app.china.presenter.LoginPresenter
import vts.app.china.utils.NetworkUtils
import vts.app.china.utils.PrefUtils
import vts.app.china.vtsview.LoginInterface
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.annotation.MainThread


/**
 * Created By Taing Sunry on 2020-02-05.
 */

class PlashScreenActivity : BaseActivity(), LoginInterface.LoginView {
    private var mLoginPresenter: LoginInterface.presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash_screen)
        setPresenter(LoginPresenter(this, this))
        startToMain()
    }

    private fun startToMain() {
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            mLoginPresenter?.onStartReqest(
                PrefUtils.getUserUKey(this).toString(),
                PrefUtils.getUserPKey(this).toString()
            )

        }, 1000)
    }

    override fun showProgressing() {

    }

    override fun hideProgressing() {

    }

    override fun onSuccessView(data: LoginResponse) {
        if (data != null && data.login) {
            PrefUtils.saveApiKey(this, "${if (data.userData != null) data.userData.token else ""}")
            PrefUtils.saveLoginKey(this, data.userData)
            startActivityMain()
        } else {
            if (PrefUtils.getLoginData(this) != null) {
                PrefUtils.signOutUser(this)
                val loginActivity =
                    Intent(this, LoginActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(loginActivity)
                this.finish()
                Toast.makeText(this, "${data.message}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun setPresenter(presenter: LoginInterface.presenter) {
        mLoginPresenter = presenter
    }

    override fun onError(t: Any) {
        startActivityMain()
    }

    override fun onErrorConnection(any: Any) {
        startActivityMain()
    }

    private fun startActivityMain() {
        if (!PrefUtils.getApiKey(this).isNullOrBlank()) {
            val homeActivity =
                Intent(this, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(homeActivity)
            this.finish()
            return
        }
        val loginActivity =
            Intent(this, LoginActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(loginActivity)
        this.finish()
    }
}