package vts.app.china.vtsui.fragment

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import vts.app.china.R
import vts.app.china.vtsui.adpater.ReviseGoodsAdapter
import android.content.Intent
import android.graphics.Color
import android.view.*
import android.widget.ProgressBar
import androidx.appcompat.widget.SearchView
import kotlinx.android.synthetic.main.activity_revise_goods.*
import vts.app.china.presenter.ReportListPresenter
import vts.app.china.vtsview.ReportListInterface
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import vts.app.china.recylerview_infinit_scroll.OnLoadMoreListener
import vts.app.china.recylerview_infinit_scroll.RecyclerViewLoadMoreScroll
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import vts.app.china.model.response.*
import androidx.recyclerview.widget.ItemTouchHelper
import com.google.android.material.snackbar.Snackbar
import vts.app.china.goodsdetail.GoodsDetailActivity
import vts.app.china.utils.NetworkUtils
import vts.app.china.utils.RecyclerItemTouchHelper


/**
 * Created By Taing Sunry on 2020-02-08.
 */

class ClearStockListFragment(var warehouse_title: String = "") : Fragment(),
    ReviseGoodsAdapter.itemListenter,
    ReportListInterface.reportView, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var reportPresenter: ReportListInterface.reportPresent
    private lateinit var viewAdapter: ReviseGoodsAdapter
    lateinit var scrollListener: RecyclerViewLoadMoreScroll

    private var mDataList = arrayListOf<ReportListData?>()
    private var page: Int = 1
    private var mDataItems: ContainerData? = null
    private var updatePosistion = 0
    private var con_code_id: String = ""
    private var textSearch: String = ""
    private var isResultCode = 0


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_clear_stock, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isResultCode = Activity.RESULT_CANCELED
        setHasOptionsMenu(true)
        setPresenter(ReportListPresenter(this, activity!!))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpList(view)

        requestData(
            search = "$textSearch",
            ware_house_title = warehouse_title,
            page = "$page",
            isMore = false
        )

        swipeRefresh?.setOnRefreshListener {
            page = 1
            requestData(
                search = "$textSearch",
                ware_house_title = warehouse_title,
                page = "$page",
                isMore = false
            )
        }
    }

    private fun requestData(
        search: String,
        ware_house_title: String,
        page: String,
        isMore: Boolean
    ) {
        if (!isMore) {
            this.page = 1
            swipeRefresh?.isRefreshing = true
        }
        reportPresenter.onLoadProductListReport(
            clear = "1",
            ware_house_title = "$ware_house_title",
            con_no_id = "",
            search = search,
            page = page,
            isMore = isMore
        )
    }

    override fun onError(throwable: Throwable) {
        tvNoDataFound?.visibility = View.VISIBLE
        swipeRefresh?.isRefreshing = false
    }

    override fun setPresenter(presenter: ReportListInterface.reportPresent) {
        reportPresenter = presenter
    }

    override fun onError(t: Any) {
        swipeRefresh?.isRefreshing = false
    }

    override fun onErrorConnection(any: Any) {
        swipeRefresh?.isRefreshing = false
    }

    override fun onIntent(data: ReportListData, position: Int) {
        updatePosistion = position
        val intentDetail = Intent(activity, GoodsDetailActivity::class.java)
        intentDetail.putExtra("data", data)
        startActivityForResult(intentDetail, 1029)
    }

    private fun setUpList(view: View) {
        viewManager = LinearLayoutManager(activity)
        viewAdapter = ReviseGoodsAdapter(mDataList!!, this, true, activity!!)
        recyclerView = view.findViewById<RecyclerView>(R.id.mRecylerListGoods).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        setRVScrollListener()
        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int, position: Int) {
        if (viewHolder is ReviseGoodsAdapter.ItemViewHolder) {
            viewHolder?.viewBackground?.findViewById<TextView>(R.id.tvClearIng)?.visibility =
                View.GONE
            viewHolder?.viewBackground?.findViewById<RelativeLayout>(R.id.startLoading)
                ?.visibility = View.VISIBLE
            if (!mDataList.isNullOrEmpty()) {
                if (!NetworkUtils.isOnline(activity!!)) {
                    val snackbar = Snackbar
                        .make(
                            coordinatorLayoutHistory,
                            "Check your internet connection!.",
                            Snackbar.LENGTH_LONG
                        )
                    snackbar.show()
                    return
                }
                val mDataLists = viewAdapter!!.getItemAtPosition(position) as ReportListData
                reportPresenter?.clearStock("", "${mDataLists?.id}", position)
            }

        }
    }

    override fun onClearStockSuccess(responseData: RemarkResponse, position: Int) {
        if (responseData?.list_nocode.id.isNullOrBlank()) {
            Toast.makeText(activity!!, "Update false", Toast.LENGTH_SHORT).show()
            val textError = recyclerView?.findViewHolderForAdapterPosition(position)
                ?.itemView?.findViewById<TextView>(R.id.delete_icon)
            val loadProgress = recyclerView?.findViewHolderForAdapterPosition(position)
                ?.itemView?.findViewById<ProgressBar>(R.id.progressBarClear)
            textError?.text = "Something error..."
            loadProgress?.visibility = View.GONE
            return
        }
        viewAdapter.removeItem(position)
        isResultCode = Activity.RESULT_OK
        val snackbar = Snackbar
            .make(
                coordinatorLayoutHistory,
                responseData?.list_nocode.kind_of_goods + "Has revised",
                Snackbar.LENGTH_LONG
            )
        snackbar.setActionTextColor(Color.YELLOW)
        snackbar.show()
        if (viewAdapter?.itemCount == 0) {
            page = 1
            requestData(
                search = "$textSearch",
                ware_house_title = warehouse_title,
                page = "$page",
                isMore = false
            )
        }
    }

    override fun onErrorClearStockModel(throwable: Throwable) {
        val snackbar = Snackbar
            .make(
                coordinatorLayoutHistory,
                "Server error!.",
                Snackbar.LENGTH_LONG
            )
        snackbar.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1029 && resultCode == Activity.RESULT_OK) {
            val remarkMore = data?.getStringExtra("data")?.toString()
            viewAdapter?.updateObjectData("$remarkMore", updatePosistion)
        }
    }

    private fun setRVScrollListener() {
        scrollListener = RecyclerViewLoadMoreScroll(viewManager as LinearLayoutManager)
        scrollListener.setOnLoadMoreListener(object :
            OnLoadMoreListener {
            override fun onLoadMore() {
                viewAdapter?.addLoadingView()
                requestData(
                    search = "$textSearch",
                    ware_house_title = warehouse_title,
                    page = "$page",
                    isMore = true
                )

            }
        })
        recyclerView.addOnScrollListener(scrollListener)
    }

    override fun onSuccess(responseData: ReportResponse) {
        mDataList?.clear()
        mDataList = responseData!!.list_nocode as ArrayList<ReportListData?>
        viewAdapter?.notifyDataSetChanged()
        viewAdapter?.updateData(mDataList)

        swipeRefresh?.isRefreshing = false
        var view = if (mDataList?.size == 0) View.VISIBLE else View.GONE
        tvNoDataFound?.visibility = view
        page++
        scrollListener?.setLoaded()
    }


    override fun onSuccessModelMore(responseData: ReportResponse) {
        viewAdapter?.removeLoadingView()
        mDataList = responseData!!.list_nocode as ArrayList<ReportListData?>
        swipeRefresh?.isRefreshing = false
        if (mDataList?.size == 0) {
            return
        }
        page++
        viewAdapter?.addData(mDataList)
        scrollListener?.setLoaded()
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val searchViewItem = menu?.findItem(R.id.action_master_search)
        val searchViewAndroidActionBar = MenuItemCompat.getActionView(searchViewItem) as SearchView

        searchViewAndroidActionBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchViewAndroidActionBar?.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                textSearch = "$newText"
                page = 1
                requestData(
                    search = "$textSearch",
                    ware_house_title = warehouse_title,
                    page = "$page",
                    isMore = false
                )
                return true
            }

        })
        super.onCreateOptionsMenu(menu, inflater)
    }


}