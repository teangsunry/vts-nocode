package vts.app.china.vtsui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import com.baoyz.actionsheet.ActionSheet
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import com.soundcloud.android.crop.Crop
import kotlinx.android.synthetic.main.activity_lclsearching.*
import vts.app.china.utils.MessageUtits
import android.net.Uri
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.io.File
import pl.aprilapps.easyphotopicker.EasyImage
import pl.aprilapps.easyphotopicker.ChooserType
import pl.aprilapps.easyphotopicker.MediaSource
import pl.aprilapps.easyphotopicker.MediaFile
import pl.aprilapps.easyphotopicker.DefaultCallback
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.collection.arrayMapOf
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import vts.app.china.database.PhotoDatabase
import vts.app.china.database.PhotoEntity
import vts.app.china.presenter.SearchLclPresenter
import vts.app.china.vtsui.adpater.PhotoAdapter
import vts.app.china.vtsview.SearchInterface
import com.google.android.material.textfield.TextInputEditText
import com.kinda.alert.KAlertDialog
import kotlinx.android.synthetic.main.item_choose_image.*
import kotlinx.android.synthetic.main.item_qrcode_add.*
import kotlinx.android.synthetic.main.items_list_product.*
import vts.app.china.R
import vts.app.china.model.postbody.SaveDataOrdering
import vts.app.china.model.response.*
import vts.app.china.utils.Const
import vts.app.china.utils.KeyboardUtils
import vts.app.china.utils.PrefUtils
import java.lang.NullPointerException
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created By Taing Sunry on 2020-01-29.
 */

class NoCodeActivity : BaseActivity(), ActionSheet.ActionSheetListener,
    PhotoAdapter.itemListenter, SearchInterface.SearchLclView {

    private var mDialog: AlertDialog? = null

    private var easyImage: EasyImage? = null
    private var arrayIndex: Int = 0
    private var arrayStringQrCode = arrayListOf<String>()
    private var arrayStringImage = arrayListOf<String>()

    private val PERMISSIONS = arrayListOf<String>(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private var mArrayPhotList = arrayListOf<PhotoEntity>()
    private lateinit var recyclerView: RecyclerView
    private var viewAdapter: PhotoAdapter? = null
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var mPresenter: SearchInterface.presenter? = null
    private var edTextView: TextInputEditText? = null

    protected var mDataProductItems: ProductData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lclsearching)

        setPresenter(SearchLclPresenter(this, this))

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        mDataProductItems = intent?.getSerializableExtra("data") as ProductData
        supportActionBar?.title = mDataProductItems?.ware_house_title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        PhotoDatabase.getInstance(this).photoDao().getDeletAll()

        setUpRecylerView()

        easyImage = EasyImage.Builder(this)
            .setChooserTitle("Pick media")
            .setCopyImagesToPublicGalleryFolder(false)
            .setChooserType(ChooserType.CAMERA_AND_GALLERY)
            .setFolderName("nodcode")
            .allowMultiple(true)
            .build()

        setDataProductInfo(mDataProductItems!!)

        addQrCodeView()
        btnChooseImage?.setOnClickListener {
            setPermission()
        }
        btnActionAdd?.setOnClickListener {
            addQrCodeView()
        }
    }

    private fun setDataProductInfo(mGoodsData: ProductData) {
        edName?.setText("${mGoodsData.customer_name}")
        edGoods?.setText("${mGoodsData.kind_of_goods}")

        //information
        val mGoodsData = mGoodsData

        if (!mGoodsData.remark.isNullOrEmpty()) {
            titleReceiptNo?.visibility = View.VISIBLE
            val title =
                if (!mGoodsData.remark.isNullOrEmpty()) "Receipt No: ${mGoodsData.remark}" else ""
            titleReceiptNo.text = "$title"
        }

        if (!mGoodsData.quantity.isNullOrEmpty() || !mGoodsData.weight.isNullOrEmpty() || !mGoodsData.width.isNullOrEmpty() || !mGoodsData.length.isNullOrEmpty()) {
            val qty =
                if (!mGoodsData.quantity.isNullOrEmpty()) "${getString(R.string.qty)} : ${mGoodsData.quantity}" else ""
            val wieght =
                if (!mGoodsData.weight.isNullOrEmpty()) "  ${getString(R.string.weight)} : ${mGoodsData.weight}" else ""

            val width =
                if (!mGoodsData.width.isNullOrEmpty()) " W: ${mGoodsData.width}" else ""
            val height =
                if (!mGoodsData.height.isNullOrEmpty()) " H: ${mGoodsData.height}" else ""
            val length =
                if (!mGoodsData.length.isNullOrEmpty()) " L: ${mGoodsData.length}" else ""

            val resultQtyWieght =
                if (!mGoodsData.quantity.isNullOrEmpty()) "$qty" else "${mGoodsData.weight}"

            tv_qty_wieght?.visibility = View.VISIBLE
            tv_qty_wieght?.text = "$resultQtyWieght $width $length $height $wieght"
        }


        if (!mGoodsData.loading_date.isNullOrEmpty()) {
            val mDateTimeShow =
                if (!mGoodsData.loading_date.isNullOrEmpty()) "${getString(R.string.date)} ${mGoodsData.loading_date} ${mGoodsData.title}" else ""
            tv_editer?.text = "$mDateTimeShow"
        }
    }

    private fun setPermission() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val permissionStorage =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val permissionWStorage =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED || permissionStorage != PackageManager.PERMISSION_GRANTED || permissionWStorage != PackageManager.PERMISSION_GRANTED) {
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Permission to access is required")
            builder.setPositiveButton(
                "OK"
            ) { _, id ->
                val settingIntent = Intent()
                settingIntent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                val uri: Uri = Uri.fromParts("package", this.packageName, null)
                settingIntent.data = uri
                this.startActivity(settingIntent)
                builder.create().dismiss()
            }
            val dialog = builder.create()
            dialog.show()
        } else {
            showBottomSheet(this)
        }
    }


    /**
     * add dynamic qrcode view
     */
    private fun addQrCodeView() {
        var mDataArrayQrCode = arrayMapOf<Int, String>()
        val inflater: LayoutInflater =
            this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewGroup: ViewGroup = findViewById(R.id.linearGroupView)
        viewGroup.layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT
        val view: ViewGroup =
            inflater.inflate(R.layout.item_qrcode, viewGroup, false) as ViewGroup
        view.findViewById<ImageView>(R.id.btnAction).setOnClickListener {
            if (viewGroup?.childCount > 1)
                viewGroup?.removeView(view)
        }
        view.findViewById<ImageView>(R.id.btnScannQrCode).setOnClickListener {
            edTextView = view.findViewById(R.id.ed_ScannQrCode)
            IntentIntegrator(this@NoCodeActivity).setRequestCode(Const.SCANNER_REQEUST_CODE)
                .initiateScan()
        }
        val mTextCompanyInPut = view.findViewById<AutoCompleteTextView>(R.id.ed_Company)

        mTextCompanyInPut.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                arrayStringQrCode.add(s.toString())
                PrefUtils.putQrCodeArrayList("qrcode", arrayStringQrCode, this@NoCodeActivity)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ArrayAdapter<String>(
                    this@NoCodeActivity,
                    android.R.layout.simple_list_item_1,
                    arrayStringQrCode
                ).also { adapter ->
                    mTextCompanyInPut.setAdapter(adapter)
                }

            }
        })


        val mTextInPut = view.findViewById<TextInputEditText>(R.id.ed_ScannQrCode)
        mTextInPut.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                textChangeEditeText(s, mTextInPut)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        viewGroup.addView(view)

        btnSave?.setOnClickListener {
//            var mDataArrayImage = arrayMapOf<Int, String>()
            KeyboardUtils.hideKeyboard(this)
            mDataArrayQrCode.clear()
//            arrayStringImage.clear()
            val mDatQrCode = arrayListOf<String>()
            for (i in 0..viewGroup?.childCount?.minus(1)) {
                val mDataCompanyText =
                    viewGroup?.getChildAt(i).findViewById<AutoCompleteTextView>(R.id.ed_Company)
                val mDataText =
                    viewGroup?.getChildAt(i).findViewById<TextInputEditText>(R.id.ed_ScannQrCode)
                mDataArrayQrCode?.put(
                    i,
                    "${mDataCompanyText?.text?.toString()}${mDataText?.text?.toString()}"
                )
            }

            mDataArrayQrCode.values.removeAll(Collections.singleton(""))
//            mArrayPhotList.forEach {
//                mDataArrayImage.put(it.id,it.photo_url)
//            }
//            mDataArrayImage.values.removeAll(Collections.singleton(""))

            mDataArrayQrCode.entries.forEach {
                mDatQrCode.add(it.value)
            }
//            println("image:$mArrayPhotList")

            val goods = edGoods!!.text!!.toString()
            val customerName = edName!!.text!!.toString()
            val textRemark = edTextRemark!!.text!!.toString()

            val mDataSave = SaveDataOrdering(
                con_id = "${mDataProductItems?.con_id}",
                con_no_id = "${mDataProductItems?.con_no_id}",
                customer_name = customerName,
                remark = textRemark,
                receip_no = mDatQrCode,
                kind_of_goods = goods,
                quantity = "${mDataProductItems?.quantity}",
                userfile = mArrayPhotList
            )
            mPresenter?.onStartSaveData(mDataSave)
        }
    }


    /**
     * setupRecylview
     */
    private fun setUpRecylerView() {
        viewManager = GridLayoutManager(this, 3)
        viewAdapter = PhotoAdapter(mArrayPhotList, this)
        recyclerView = findViewById<RecyclerView>(R.id.photoRecylerView).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    /**
     * remove items photos
     */
    override fun onRemove(position: Int, index: Int) {
        PhotoDatabase.getInstance(this).photoDao().deleteById(index)
        mArrayPhotList =
            PhotoDatabase.getInstance(this).photoDao().getAll() as ArrayList<PhotoEntity>
        viewAdapter?.adapterRemove(position)
        isViewNoImage(mArrayPhotList.size)
    }

    override fun onOtherButtonClick(actionSheet: ActionSheet?, index: Int) {
        if (!hasPermissions(this, Manifest.permission.CAMERA) || !hasPermissions(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) || !hasPermissions(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        ) {
            ActivityCompat.requestPermissions(
                this,
                PERMISSIONS.toTypedArray(),
                Const.PERMISSION_REQUEST_CODE
            )
            return
        }

        if (index == 0) {
            easyImage?.openCameraForImage(this)
        } else if (index == 1) {
            Crop.pickImage(this)

        }
    }

    override fun onDismiss(actionSheet: ActionSheet?, isCancel: Boolean) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != Const.SCANNER_REQEUST_CODE && requestCode != IntentIntegrator.REQUEST_CODE) {
            if (requestCode == Const.REQUEST_IMAGE_CAPTURE) {
                easyImage!!.handleActivityResult(
                    requestCode,
                    resultCode,
                    data,
                    this,
                    object : DefaultCallback() {
                        override fun onMediaFilesPicked(
                            imageFiles: Array<MediaFile>,
                            source: MediaSource
                        ) {
                            beginCrop(Uri.fromFile(imageFiles[0].file))
                        }

                        override fun onImagePickerError(@NonNull error: Throwable, @NonNull source: MediaSource) {
                            error.printStackTrace()
                        }

                        override fun onCanceled(@NonNull source: MediaSource) {
                        }
                    })
            }
            if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
                data!!.data?.let { beginCrop(it) }
            } else if (requestCode == Crop.REQUEST_CROP) {
                if (data == null) return
                handleCrop(resultCode, data)
            }
            super.onActivityResult(requestCode, resultCode, data)
        } else {
            val qRCodeResult =
                IntentIntegrator.parseActivityResult(resultCode, data) as IntentResult
            if (qRCodeResult.contents != null) {
                edTextView?.setText("${qRCodeResult.contents}")
            }
        }
    }

    private fun beginCrop(source: Uri) {
        val destination = Uri.fromFile(File(cacheDir, "cropped-${arrayIndex++}.jpg"))
        Crop.of(source, destination).withAspect(640,244).start(this)
    }

    private fun handleCrop(resultCode: Int, result: Intent) {
        if (resultCode == Activity.RESULT_OK) {
            PhotoDatabase.getInstance(this).photoDao().insertAll(
                PhotoEntity(
                    id = arrayIndex++,
                    photo_url = Crop.getOutput(result).toFile().absolutePath
                )
            )
            mArrayPhotList =
                PhotoDatabase.getInstance(this).photoDao().getAll() as ArrayList<PhotoEntity>
            viewAdapter?.updateData(mArrayPhotList)
            isViewNoImage(mArrayPhotList.size)
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun isViewNoImage(dataSize: Int) {
        if (dataSize != 0) {
            tvNoImage?.visibility = View.GONE
            recyclerView?.visibility = View.VISIBLE
        } else {
            tvNoImage?.visibility = View.VISIBLE
            recyclerView?.visibility = View.GONE
        }
    }

    override fun setPresenter(presenter: SearchInterface.presenter) {
        mPresenter = presenter
    }

    override fun showProgressing() {
        mDialog = MessageUtits.showProgressbar(this)
        mDialog!!.show()
    }

    override fun hideProgressing() {
        try {
            if (!mDialog!!.isShowing || mDialog != null) {
                mDialog!!.dismiss()
                mDialog = null
            }
        } catch (e: NullPointerException) {

        }

    }

    override fun onSaveSuccessView(data: SaveDataResponse) {
        if (data.save_status || data.save_status.equals("true")) {

            KAlertDialog(this@NoCodeActivity, KAlertDialog.SUCCESS_TYPE)
                .setTitleText(getString(R.string.save_success))
                .setContentText(getString(R.string.do_continue))
                .setConfirmClickListener {
                    it?.dismiss()
                    this@NoCodeActivity.setResult(Activity.RESULT_OK)
                    this.finish()
                }
                .show()
        } else {
            KAlertDialog(this@NoCodeActivity, KAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.oops))
                .setContentText(getString(R.string.something_wrong))
                .show()
        }
        mDialog!!.dismiss()
    }

    override fun onSaveError(throwable: Any) {
        if (mDialog != null) {
            mDialog!!.dismiss()
            KAlertDialog(this, KAlertDialog.ERROR_TYPE)
                .setTitleText(getString(R.string.oops))
                .setContentText("$throwable")
                .show()
        }

    }

    override fun onError(t: Any) {
        println("error:${t}")
    }

    override fun onErrorConnection(any: Any) {

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


}