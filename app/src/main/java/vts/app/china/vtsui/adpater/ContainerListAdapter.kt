package vts.app.china.vtsui.adpater

import android.content.Context
import android.graphics.Color
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_container.view.*
import vts.app.china.R
import vts.app.china.model.response.ContainerData
import vts.app.china.recylerview_infinit_scroll.Constant


var selected_position = 0

class ContainerListAdapter(
    private var myDataset: ArrayList<ContainerData?>?,
    var mItemListenter: itemListenter,
    var context: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    fun addData(dataViews: ArrayList<ContainerData?>) {
        this.myDataset?.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun updateData(dataViews: ArrayList<ContainerData?>) {
        this.myDataset = dataViews
        notifyDataSetChanged()
    }

    fun getItemAtPosition(position: Int): ContainerData? {
        return myDataset!![position]
    }

    fun addLoadingView() {
        //Add loading item
        Handler().post {
            myDataset?.add(null)
            notifyItemInserted(myDataset?.size!! - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        if (myDataset?.size != 0) {
            myDataset?.removeAt(myDataset?.size!! - 1)
            notifyItemRemoved(myDataset!!.size)
        }
    }

    fun removeItems(position: Int) {
        //Remove loading item
        if (myDataset?.size != 0) {
            myDataset?.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, myDataset!!.size)
        }
    }

    fun updateValue(value: String, position: Int) {
        //Remove loading item
        if (myDataset?.size != 0) {
            myDataset!![position]!!.numbers = value
            notifyItemChanged(position)
            notifyItemRangeChanged(position, myDataset!!.size)
        }
    }

    fun updateValue(value: Boolean, position: Int) {
        //Remove loading item
        if (myDataset?.size != 0) {
            myDataset!![position]!!.selected_items = value
            notifyItemChanged(position)
            notifyItemRangeChanged(position, myDataset!!.size)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_container, parent, false)
            ItemViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(context).inflate(R.layout.progress_loading, parent, false)
            LoadingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return myDataset!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (myDataset!![position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            val mContainerData = myDataset!![position] as ContainerData
            holder?.itemView?.tvWarehouse_Title?.text = "${mContainerData.ware_house_title}"
            holder?.itemView?.tvloadingDatae?.text =
                "Loading date:${mContainerData.loading_date}${mContainerData.title}"
            holder?.itemView?.tvNumberStock?.text = "${mContainerData.numbers}"

            if (mContainerData.selected_items) {
                if (selected_position === position) {
                    holder.itemView?.container_color_bg?.setBackgroundColor(Color.parseColor("#0288d1"))
                    holder.itemView?.tvWarehouse_Title?.setTextColor(Color.parseColor("#ffffff"))
                    holder.itemView?.tvloadingDatae?.setTextColor(Color.parseColor("#ffffff"))
                } else {
                    holder.itemView?.container_color_bg?.setBackgroundColor(Color.parseColor("#4fc3f7"))
                    holder.itemView?.tvWarehouse_Title?.setTextColor(Color.parseColor("#ffffff"))
                    holder.itemView?.tvloadingDatae?.setTextColor(Color.parseColor("#ffffff"))
                }
            } else {
                holder.itemView?.container_color_bg?.setBackgroundColor(Color.TRANSPARENT)
                holder.itemView?.tvWarehouse_Title?.setTextColor(Color.parseColor("#000000"))
                holder.itemView?.tvloadingDatae?.setTextColor(Color.parseColor("#70000000"))
            }

            holder.itemView?.setOnClickListener {
                if (mItemListenter != null) {
                    mItemListenter?.onIntent(myDataset!![position]!!, position)
                    updateValue(true, position)
                    notifyItemChanged(selected_position)
                    selected_position = position
                    notifyItemChanged(selected_position)
                }
            }

//            if (tracker!!.isSelected(position.toLong())) {
//                holder?.itemView.background = ColorDrawable(
//                    Color.parseColor("#80deea")
//                )
//            }
//            else {
//                holder?.itemView.background = ColorDrawable(Color.WHITE)
//            }
        }

    }


    interface itemListenter {
        fun onIntent(data: ContainerData, position: Int)
    }
}