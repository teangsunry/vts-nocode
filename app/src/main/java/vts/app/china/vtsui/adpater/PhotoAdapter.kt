package vts.app.china.vtsui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.items_photo.view.*
import vts.app.china.R
import vts.app.china.database.PhotoEntity

class PhotoAdapter(private var myDataset: ArrayList<PhotoEntity>, var mItemListenter: itemListenter) :
    RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PhotoViewHolder {
        return PhotoViewHolder(
            LayoutInflater.from(parent?.context).inflate(
                R.layout.items_photo,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = myDataset?.size

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        if (holder is PhotoViewHolder) {
            holder.bindData(myDataset[position], position, mItemListenter)
        }
    }

    fun updateData(mDataList: ArrayList<PhotoEntity>) {
        myDataset = mDataList
        notifyDataSetChanged()
    }

    fun adapterRemove(position: Int){
        myDataset.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position,myDataset.size)
    }

    class PhotoViewHolder(viewHolder: View) : RecyclerView.ViewHolder(viewHolder) {
        fun bindData(myDataset: PhotoEntity, position: Int, mItemListenter: itemListenter) {
            itemView?.imageViewPhoto?.setImageURI(myDataset.photo_url.toUri())
            itemView?.imgRemove?.setOnClickListener {
                if (mItemListenter != null) {
                    mItemListenter?.onRemove(position,myDataset.id)
                }
            }
        }
    }

    interface itemListenter {
        fun onRemove(position: Int,arrid:Int)
    }
}