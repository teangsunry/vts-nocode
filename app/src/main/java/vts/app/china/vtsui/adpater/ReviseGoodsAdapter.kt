package vts.app.china.vtsui.adpater

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.items_history.view.tvName
import kotlinx.android.synthetic.main.items_revise_goods.view.*
import vts.app.china.R
import vts.app.china.model.response.ReportListData
import vts.app.china.recylerview_infinit_scroll.Constant


class ReviseGoodsAdapter(
    private var myDataset: ArrayList<ReportListData?>?,
    var mItemListenter: itemListenter,
    var isClear: Boolean = false,
    var context: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var viewBackground: LinearLayout
        var viewForeground: RelativeLayout

        init {
            viewBackground = itemView.findViewById(R.id.view_background)
            viewForeground = itemView.findViewById(R.id.view_foreground)
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    fun addData(dataViews: ArrayList<ReportListData?>) {
        this.myDataset?.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun updateData(dataViews: ArrayList<ReportListData?>) {
        this.myDataset = dataViews
        notifyDataSetChanged()
    }

    fun updateObjectData(mRemarkMore: String, position: Int) {
        this.myDataset!![position]!!.remark_more = mRemarkMore
        notifyItemChanged(position)
        notifyItemRangeChanged(position, myDataset!!.size)
    }

    fun getItemAtPosition(position: Int): ReportListData? {
        return myDataset!![position]
    }

    fun addLoadingView() {
        //Add loading item
        Handler().post {
            myDataset?.add(null)
            notifyItemInserted(myDataset?.size!! - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        if (myDataset?.size != 0) {
            myDataset?.removeAt(myDataset?.size!! - 1)
            notifyItemRemoved(myDataset!!.size)
        }
    }

    fun removeItem(position: Int) {
        myDataset?.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: ReportListData, position: Int) {
        myDataset?.add(position, item)
        // notify item added by position
        notifyItemInserted(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.items_revise_goods, parent, false)
            ItemViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(context).inflate(R.layout.progress_loading, parent, false)
            LoadingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return myDataset!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (myDataset!![position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    fun loadImage(part: String, imageString: String): String {
        var urlImage = imageString

        if (!urlImage.isNullOrBlank()) {
            val items = imageString?.split(",")
            urlImage = part + if (!items[0].isNullOrBlank()) items[0] else items[1]
        }
        return urlImage
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {

            if (isClear) {
                holder.itemView?.tvClearIng?.text =
                    holder?.itemView?.context?.getString(R.string.revise)
            }

            val mGoodsData = myDataset!![position] as ReportListData
            val imageUrl = "${loadImage(mGoodsData?.part, mGoodsData?.prod_image)}"
            if (!imageUrl.isNullOrBlank()) {
                holder.itemView?.loadinImage?.visibility = View.VISIBLE

                Picasso.get()
                    .load(imageUrl)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .resize(200, 200)
                    .into(holder.itemView.imgGallery, object : Callback {
                        override fun onSuccess() {
                            holder.itemView?.loadinImage?.visibility = View.GONE
                        }

                        override fun onError(e: Exception?) {
                            holder.itemView?.loadinImage?.visibility = View.GONE
                            holder.itemView.imgGallery.setImageResource(R.drawable.error_image)
                        }
                    })

            }

            if (!mGoodsData.receip_no.isNullOrEmpty()) {
                holder.itemView.titleReceiptNo?.visibility = View.VISIBLE
                val title = "Receipt No: ${mGoodsData.receip_no}"
                holder.itemView.titleReceiptNo.text = "$title"
            }

            if (!mGoodsData?.code_id.isNullOrBlank()) {
                holder.itemView.tvCustomerCode?.visibility = View.VISIBLE
                val mCustomerCode =
                    if (!mGoodsData.code_id.isNullOrEmpty()) "${mGoodsData.code_id}" else ""
                holder.itemView.tvCustomerCode.text = "$mCustomerCode"
            }

            if (!mGoodsData?.customer_name.isNullOrBlank()) {
                holder.itemView.tvCustomerName?.visibility = View.VISIBLE
                val mCustomerName =
                    if (!mGoodsData.customer_name.isNullOrEmpty()) "${mGoodsData.customer_name}" else ""
                holder.itemView.tvCustomerName.text = "$mCustomerName"
            }

            if (!mGoodsData.kind_of_goods.isNullOrEmpty()) {
                holder.itemView?.tvName?.visibility = View.VISIBLE
                holder.itemView?.tvName?.text =
                    "${context.getString(R.string.goods)} : ${mGoodsData.kind_of_goods}"
            }

            if (!mGoodsData.quantity.isNullOrEmpty() || !mGoodsData.weight.isNullOrEmpty() || !mGoodsData.width.isNullOrEmpty() || !mGoodsData.length.isNullOrEmpty()) {
                val qty =
                    if (!mGoodsData.quantity.isNullOrEmpty()) "${context.getString(R.string.qty)} : ${mGoodsData.quantity}" else ""

                val width =
                    if (!mGoodsData.width.isNullOrEmpty()) " W: ${mGoodsData.width}" else ""
                val height =
                    if (!mGoodsData.height.isNullOrEmpty()) " H: ${mGoodsData.height}" else ""
                val length =
                    if (!mGoodsData.length.isNullOrEmpty()) " L: ${mGoodsData.length}" else ""

                val resultQtyWieght =
                    if (!mGoodsData.quantity.isNullOrEmpty()) "$qty" else ""

                holder.itemView?.tv_qty_wieght?.visibility = View.VISIBLE
                holder.itemView?.tv_qty_wieght?.text = "$resultQtyWieght $width $length $height"

            }

            if (!mGoodsData.weight.isNullOrEmpty()) {
                holder.itemView?.tv_wieght?.visibility = View.VISIBLE
                val wieght =
                    if (!mGoodsData.weight.isNullOrEmpty()) " ${context.getString(R.string.weight)}: ${mGoodsData.weight}" else ""
                holder.itemView?.tv_wieght?.text = "$wieght"
            }

            if (!mGoodsData.loading_date.isNullOrEmpty() || !mGoodsData.title.isNullOrEmpty()) {
                holder.itemView?.tv_loading_date?.visibility = View.VISIBLE
                val mDateTimeShow =
                    if (!mGoodsData.loading_date.isNullOrEmpty()) "${context.getString(R.string.date)} ${mGoodsData.loading_date} ${mGoodsData.title}" else ""

                holder.itemView?.tv_loading_date?.text = "$mDateTimeShow"
            }

            if (!mGoodsData.created_time.isNullOrEmpty() || !mGoodsData.user.isNullOrEmpty()) {
                holder.itemView?.titleCreate?.visibility = View.VISIBLE
                val mCreateDateUsers =
                    if (!mGoodsData.created_time.isNullOrEmpty()) "${context.getString(R.string.c_date)} ${mGoodsData.created_time}" else ""

                holder.itemView?.titleCreate?.text = "$mCreateDateUsers"
            }

            if (!mGoodsData.user.isNullOrEmpty()) {
                holder.itemView?.titleUser?.visibility = View.VISIBLE
                val mUser = if (!mGoodsData.user.isNullOrBlank()) "User:${mGoodsData.user} " else ""
                holder.itemView?.titleUser?.text = "$mUser"
            }

            if (!mGoodsData.remark.isNullOrEmpty()) {
                holder.itemView?.titleRemark?.visibility = View.VISIBLE
                val remark =
                    if (!mGoodsData.remark.isNullOrEmpty()) "${context.getString(R.string.remark)}: ${mGoodsData.remark}" else ""
                holder.itemView?.titleRemark?.text = "$remark"
            }

            if (!mGoodsData.remark_more.isNullOrEmpty()) {
                holder.itemView?.titleRemarkMore?.visibility = View.VISIBLE
                holder.itemView?.titleRemarkMore?.text =
                    "Remark more : ${mGoodsData.remark_more}"
            }

            if (!mGoodsData.remark_no.isNullOrEmpty()) {
                holder.itemView?.titleRemarkNo?.visibility = View.VISIBLE
                holder.itemView?.titleRemarkNo?.text =
                    "Remark No : ${mGoodsData.remark_no}"
            }

            holder.itemView.view_foreground?.setOnClickListener {
                if (mItemListenter != null) {
                    mItemListenter?.onIntent(myDataset!![position]!!, position)
                }
            }
        }

    }

    interface itemListenter {
        fun onIntent(data: ReportListData, position: Int)
    }
}