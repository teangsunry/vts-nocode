package vts.app.china.vtsui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.widget.Toolbar
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.ui.AppBarConfiguration
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import vts.app.china.R
import vts.app.china.utils.LanguageManager
import vts.app.china.utils.LocaleHelper
import vts.app.china.utils.PrefUtils
import vts.app.china.vtsui.fragment.*


/**
 * Created By Taing Sunry on 2020-01-29.
 */

class MainActivity : BaseActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var drawerLayout: DrawerLayout
    lateinit var navigationView: NavigationView
    lateinit var toolbar: Toolbar
    lateinit var toggle: ActionBarDrawerToggle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)

        updateToolbar()
        setUpNavigation()

        onContentChange(R.id.navHome)

    }


    fun onContentChange(res: Int) {
        var fragment: Fragment? = null
        when (res) {
            R.id.navHome -> {
                fragment = HomeFragment()
                supportActionBar?.title = getString(R.string.home)
            }

            R.id.navReport -> {
                fragment = ReportWareHouseFragment()
                supportActionBar?.title = getString(R.string.report)
            }

            R.id.container -> {
                fragment = ContainerFragment()
                supportActionBar?.title = getString(R.string.container)

            }
            R.id.navOut -> {
//                fragment = ClearStockListFragment()
                fragment = ClearStockWareHouseFragment()
                supportActionBar?.title = getString(R.string.clear_stock)

            }
        }

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.mContenerFrame, fragment!!)
        transaction.commit()

    }

    override fun onBackPressed() {
        onBackHomeActivity()
        super.onBackPressed()
    }

    private fun onBackHomeActivity() {
        val index = supportFragmentManager.backStackEntryCount - 1
        if (index >= 0) {
            val backEntry = supportFragmentManager.getBackStackEntryAt(index)
            val tag = backEntry.name
            if (tag.equals(getString(R.string.home))) {
                supportActionBar?.title = getString(R.string.home)
                updateToolbar()
            }
            if (tag.equals(getString(R.string.report))) {
                supportActionBar?.title = getString(R.string.report)
                updateToolbar()
            }
            if (tag.equals(getString(R.string.clear_stock))) {
                supportActionBar?.title = getString(R.string.clear_stock)
                updateToolbar()
            }
        }
    }


    private fun updateToolbar() {
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navHome,
                R.id.navReport
            ), drawerLayout
        )
        toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        drawerLayout.addDrawerListener(toggle)

        toggle.syncState()
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    /**
     * setup navigation drawer
     */
    private fun setUpNavigation() {
        navigationView = findViewById(R.id.nav_view)
        val imgIcon =
            if (LanguageManager.getInstance(this).language == "en") R.drawable.ic_china_flag else R.drawable.ic_united_kingdom_flage
        navigationView.iconFlage?.setImageResource(imgIcon)

        updateTextDrawer(navigationView)

        navigationView.loginBtn?.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START))
                if (PrefUtils.getApiKey(this) == null) {
                    drawer_layout.closeDrawer(GravityCompat.START)
                    val intentLogin = Intent(this, LoginActivity::class.java)
                    startActivity(intentLogin)
                    return@setOnClickListener
                }
            PrefUtils.signOutUser(this)
            updateTextDrawer(navigationView)
        }
        navigationView.languageBtn?.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START))
                drawer_layout.closeDrawer(GravityCompat.START)

            val languageResult =
                if (LanguageManager.getInstance(this).language == "zh") "en" else "zh"
            LanguageManager.getInstance(this).saveLanguage(languageResult)

            updateViews(languageResult)

        }
        navigationView.navHome?.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START))
                drawer_layout.closeDrawer(GravityCompat.START)
            onContentChange(R.id.navHome)

        }
        navigationView.navReport?.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START))
                drawer_layout.closeDrawer(GravityCompat.START)
            onContentChange(R.id.navReport)

        }

        navigationView.navOut?.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START))
                drawer_layout.closeDrawer(GravityCompat.START)
            onContentChange(R.id.navOut)

        }

        navigationView?.tvVersionCode?.text = "App Version:${appVersion(this)}"
    }

    private fun updateViews(languageCode: String) {
        val context = LocaleHelper.setLocale(this, languageCode)
        val resources = context.resources
        val imgIcon =
            if (LanguageManager.getInstance(this).language == "en") R.drawable.ic_china_flag else R.drawable.ic_united_kingdom_flage
        nav_view.iconFlage?.setImageResource(imgIcon)
        tvlanguage.text = resources.getString(R.string.language)
        tv_login?.text = resources.getString(PrefUtils.isLoginText(context))
    }

    /**
     * update text navigation drawer
     */
    private fun updateTextDrawer(navView: NavigationView) {
        navView.icon_login?.setImageResource(PrefUtils.isLoginIcon(this))
        navView.tv_login?.setText(getString(PrefUtils.isLoginText(this)))
        val whTitle = navView.getHeaderView(0).findViewById<TextView>(R.id.tvWhTitle)
        whTitle.text = "${if (!PrefUtils.getLoginData(this)!!.username.isNullOrEmpty()) {
            "User:(${cap1stChar(PrefUtils.getLoginData(this)!!.username)})"
        } else {
            ""
        }}"
        val user = navView.getHeaderView(0).findViewById<TextView>(R.id.tvRound)
        user.text = if (!PrefUtils.getLoginData(this)!!.title_warehouse.isNullOrEmpty()){
            "${cap1stChar(PrefUtils.getLoginData(this)!!.title_warehouse).subSequence(0, 1)}(${ cap1stChar(PrefUtils.getLoginData(this)!!.username).subSequence(0, 1)})"
        }else{""}
    }

    fun cap1stChar(userIdea: String): String {
        var userIdea = userIdea
        val stringArray = userIdea.toCharArray()
        if (stringArray.isNotEmpty())
            stringArray[0] = Character.toUpperCase(stringArray[0])
        userIdea = String(stringArray)
        return userIdea
    }

}
