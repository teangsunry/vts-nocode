package vts.app.china.vtsui

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.text.Editable
import android.view.MotionEvent
import android.view.View
import android.widget.AutoCompleteTextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.baoyz.actionsheet.ActionSheet
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.textfield.TextInputEditText
import vts.app.china.R
import vts.app.china.utils.LocaleHelper
import vts.app.china.utils.PrefUtils




/**
 * Created By Taing Sunry on 2020-01-29.
 */

open class BaseActivity : AppCompatActivity() {

    protected val DATA_SAVESTATE="DATA_SAVESTATE"


    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase!!))
    }

    fun hasPermissions(context: Context, vararg permissions: String): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
        }

    fun showBottomSheet(listenter: ActionSheet.ActionSheetListener) {
        val mActionBottonSheet = ActionSheet.createBuilder(this, supportFragmentManager)
        mActionBottonSheet.setCancelButtonTitle(this.getString(R.string.cancel))
        mActionBottonSheet.setOtherButtonTitles("Camera", this.getString(R.string.photo_gallery))
        mActionBottonSheet.setCancelableOnTouchOutside(true)
        mActionBottonSheet.setListener(listenter).show()
    }

    fun textChangeEditeText(s: Editable?, autoCompleteTextView: TextInputEditText) {
        if (s!!.isNotEmpty()) {
            autoCompleteTextView?.setCompoundDrawablesWithIntrinsicBounds(
                null, null,
                getDrawable(R.drawable.ic_clear_black_24dp), null
            )

            autoCompleteTextView?.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                    if (event?.action == MotionEvent.ACTION_UP) {
                        if (s.isNotBlank())
                            if (event?.rawX >= (autoCompleteTextView?.right!!.minus(
                                    autoCompleteTextView?.compoundDrawables!![2].bounds.width()
                                ))
                            ) {
                                autoCompleteTextView?.setText("")
                                s?.clear()
                                return true
                            }
                    }
                    return false
                }
            })
        } else {
            autoCompleteTextView?.setCompoundDrawablesWithIntrinsicBounds(
                null, null,
                null, null
            )
        }
    }


    protected fun onCollapsingToolbarListener(
        mAppBar: AppBarLayout,
        mCollapsingToolbarLayout: CollapsingToolbarLayout,
        title: String, mCalendarToolbarListener: CalendarToolbarListener
    ) {
        mCollapsingToolbarLayout.title = ""
        var isShow = true
        var scrollRange = -1
        mAppBar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { barLayout, verticalOffset ->
            if (scrollRange == -1) {
                scrollRange = barLayout?.totalScrollRange!!
            }
            if (scrollRange + verticalOffset == 0) {
                mCollapsingToolbarLayout.title = title
                isShow = true
            } else if (isShow) {
                mCollapsingToolbarLayout.title = ""
                isShow = false
            }
            mCalendarToolbarListener?.onToolbarCalender(isShow)
        })
    }

    interface CalendarToolbarListener {
        fun onToolbarCalender(isShow: Boolean)
    }

    fun appVersion(context: Context):String{
        var versionName:String=""
        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            val version = pInfo.versionName
            val verCode = pInfo.versionCode
            versionName =pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return versionName

    }
}