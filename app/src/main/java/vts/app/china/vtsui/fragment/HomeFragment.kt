package vts.app.china.vtsui.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.content_main.*
import vts.app.china.R
import vts.app.china.model.response.WareHouseData
import vts.app.china.model.response.WareHouseItems
import vts.app.china.presenter.WareHousePresenter
import vts.app.china.utils.NetworkUtils
import vts.app.china.vtsui.adpater.WareHouseAdapter
import vts.app.china.vtsview.WareHouseInterface
import vts.app.china.vtsui.MainActivity


/**
 * Created By Taing Sunry on 2020-02-21.
 */

class HomeFragment : Fragment(), WareHouseAdapter.itemListenter,
    WareHouseInterface.warehouseView {

    private var mDataList = arrayListOf<WareHouseItems>()
    private var viewAdapter: WareHouseAdapter? = null
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var mPresenter: WareHouseInterface.warehousePresenter? = null
    private var mContext: Activity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = activity
        setPresenter(WareHousePresenter(this, mContext!!))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.content_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onSetUpRecylerViewAdapter(view = view)

        onLoadData()

        mSwepRefresh?.setOnRefreshListener {
            onLoadData()
        }

    }

    override fun onSuccess(responseData: WareHouseData) {
        tvNoData?.visibility = View.GONE
        mSwepRefresh?.isRefreshing = false
        mDataList.clear()
        mDataList = responseData?.group_nocode
        if (mDataList?.size == 0) {
            tvNoData?.visibility = View.VISIBLE
            tvNoData?.text = "No stock"
        }
        viewAdapter?.updateData(mDataList)
    }

    override fun onError(throwable: Throwable) {
        tvNoData?.visibility = View.VISIBLE
        mSwepRefresh?.isRefreshing = false
        Toast.makeText(mContext, "${throwable.message}", Toast.LENGTH_SHORT).show()
    }

    override fun setPresenter(presenter: WareHouseInterface.warehousePresenter) {
        mPresenter = presenter
    }

    override fun onError(t: Any) {
        tvNoData?.visibility = View.VISIBLE
        mSwepRefresh?.isRefreshing = false
        Toast.makeText(mContext, "${t?.toString()}", Toast.LENGTH_SHORT).show()
    }

    override fun onErrorConnection(any: Any) {
        tvNoData?.visibility = View.VISIBLE
        mSwepRefresh?.isRefreshing = false
        Toast.makeText(mContext, "${any?.toString()}", Toast.LENGTH_SHORT).show()
    }

    private fun onLoadData() {
        mSwepRefresh?.isRefreshing = true
        mPresenter?.onLoadData()
    }

    override fun onResume() {
        super.onResume()
        onLoadData()
    }

    private fun onSetUpRecylerViewAdapter(view: View) {
        viewManager = LinearLayoutManager(mContext)
        viewAdapter = WareHouseAdapter(mDataList, this)
        recyclerView = view.findViewById<RecyclerView>(R.id.mRecylerViewWarehouse).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun onIntent(data: WareHouseItems) {
        if (!NetworkUtils.isOnline(mContext!!)) {
            Toast.makeText(mContext, "No internet connection!", Toast.LENGTH_SHORT).show()
            return
        }

        val mYourActiviy = activity as MainActivity?
        mYourActiviy?.supportActionBar?.title =  "${getString(R.string.home)}->${getString(R.string.container)}"
        mYourActiviy?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        mYourActiviy?.toolbar?.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        mYourActiviy?.toolbar?.setNavigationOnClickListener {
            mYourActiviy?.onBackPressed()
        }
        val transaction = activity!!.supportFragmentManager.beginTransaction()
        transaction.replace(
            R.id.mContenerFrame,
            ContainerFragment.newInstance("${data.ware_house_title}")
        )
            .addToBackStack(getString(R.string.home))
        transaction.commit()
    }

}