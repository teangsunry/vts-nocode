package vts.app.china.vtsui.adpater

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_warehouse.view.*
import vts.app.china.R
import vts.app.china.model.response.WareHouseItems

class WareHouseAdapter(
    private var myDataset: ArrayList<WareHouseItems>,
    var mItemListenter: itemListenter
) :
    RecyclerView.Adapter<WareHouseAdapter.WareHouseViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WareHouseViewHolder {
        return WareHouseViewHolder(
            LayoutInflater.from(parent?.context).inflate(
                R.layout.item_warehouse,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = myDataset?.size

    override fun onBindViewHolder(holder: WareHouseViewHolder, position: Int) {
        val mDataItem = myDataset[position]
        if (holder is WareHouseViewHolder) {
            holder.bindData(mDataItem, mItemListenter)
        }
    }

    fun updateData(mDataList: ArrayList<WareHouseItems>) {
        myDataset = mDataList
        notifyDataSetChanged()
    }

    class WareHouseViewHolder(viewHolder: View) : RecyclerView.ViewHolder(viewHolder) {
        fun bindData(mWareHouseData: WareHouseItems, mItemListenter: itemListenter) {
            itemView?.tvWarehouse_Title?.text = "${mWareHouseData?.ware_house_title}"
            itemView?.tvNumberStock?.text = "${mWareHouseData?.numbers}"
            itemView?.setOnClickListener {
                if (mItemListenter != null) {
                    mItemListenter?.onIntent(mWareHouseData)
                }
            }
        }
    }

    interface itemListenter {
        fun onIntent(data: WareHouseItems)
    }
}