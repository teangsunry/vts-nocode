package vts.app.china.vtsui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.kinda.alert.KAlertDialog
import kotlinx.android.synthetic.main.fragment_dialog_add_remark.view.*
import vts.app.china.R
import vts.app.china.model.response.RemarkResponse
import vts.app.china.presenter.RemakrPresenter
import vts.app.china.utils.MessageUtits
import vts.app.china.utils.NetworkUtils
import vts.app.china.vtsview.RemarkInterface

/**
 * Created By Taing Sunry on 2020-02-26.
 */

class AddRemarkFragmentDialog(val param: String, var myRemark: String) : DialogFragment(),
    RemarkInterface.remarkView {


    lateinit var presenterRemark: RemarkInterface.remarkPresenter
    private var mDialog: AlertDialog? = null
    var mCallback: OnFragmentInteractionListener? = null

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(responseData: RemarkResponse)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            mCallback = activity as OnFragmentInteractionListener?
        } catch (e: Exception) {
            println("error:$e")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return View.inflate(context, R.layout.fragment_dialog_add_remark, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setPresenter(RemakrPresenter(this, view.context))
        println("id::$myRemark")
        view?.edRemarkMore?.setText("$myRemark")
        view?.btnSaveRemark?.setOnClickListener {
            val remarkEd = view?.edRemarkMore?.text?.toString()

            if (!NetworkUtils.isOnline(view.context)) {
                Toast.makeText(view.context, "No internet connection", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (remarkEd.isNullOrBlank()) {
                Toast.makeText(view.context, "Please write something...", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!remarkEd.isNullOrBlank() && NetworkUtils.isOnline(view.context)) {
                mDialog = MessageUtits.showProgressbar(context!!)
                mDialog!!.show()
                presenterRemark?.onSaveRemark(remarkEd, "$param")
                return@setOnClickListener
            }

        }
    }


    override fun onSuccess(responseData: RemarkResponse) {
        if (mDialog != null) {
            mDialog!!.dismiss()
            mDialog = null
        }
        checkNotNull(context)
        KAlertDialog(context!!, KAlertDialog.SUCCESS_TYPE)
            .setTitleText(getString(R.string.save_success))
            .setContentText(getString(R.string.do_continue))
            .setConfirmClickListener {
                it?.dismiss()
                mCallback?.onFragmentInteraction(responseData)
                this?.dismiss()
            }
            .show()
    }

    override fun onError(throwable: Throwable) {
        if (mDialog != null) {
            mDialog!!.dismiss()
            mDialog = null
        }
        checkNotNull(context)
        KAlertDialog(context!!, KAlertDialog.ERROR_TYPE)
            .setTitleText(getString(R.string.oops))
            .setContentText(getString(R.string.something_wrong))
            .show()
    }

    override fun setPresenter(presenter: RemarkInterface.remarkPresenter) {
        presenterRemark = presenter
    }

    override fun onError(t: Any) {
        if (mDialog != null) {
            mDialog!!.dismiss()
            mDialog = null
        }
        checkNotNull(context)
        KAlertDialog(context!!, KAlertDialog.ERROR_TYPE)
            .setTitleText(getString(R.string.oops))
            .setContentText(getString(R.string.something_wrong))
            .show()
    }

    override fun onErrorConnection(any: Any) {

    }
}