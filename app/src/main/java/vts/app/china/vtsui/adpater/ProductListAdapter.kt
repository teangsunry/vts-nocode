package vts.app.china.vtsui.adpater

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.items_list_product.view.*
import vts.app.china.R
import vts.app.china.model.response.ProductData
import vts.app.china.recylerview_infinit_scroll.Constant

class ProductListAdapter(
    var myDataset: ArrayList<ProductData?>?,
    var mItemListenter: itemListenter,
    var context: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun clearData(dataViews: ArrayList<ProductData?>) {
        this.myDataset?.removeAll(dataViews)
        this.notifyDataSetChanged()
    }


    fun addData(dataViews: ArrayList<ProductData?>) {
        this.myDataset?.clear()
        this.myDataset?.addAll(dataViews)
        this.notifyDataSetChanged()
    }

    fun addDataMore(dataViews: ArrayList<ProductData?>) {
        this.myDataset?.addAll(dataViews)
        this.notifyDataSetChanged()
    }

    fun addDataIndex(position: Int, dataViews: ProductData) {
        this.myDataset?.add(position, dataViews)
//        notifyItemChanged(position)
        notifyItemRangeChanged(position, myDataset!!.size)
    }

//    fun updateData(dataViews: ArrayList<ProductData?>) {
//        this.myDataset = dataViews
//        this.notifyDataSetChanged()
//    }

    fun getItemAtPosition(position: Int): ProductData? {
        return myDataset!![position]
    }

    fun addLoadingView() {
        //Add loading item
        Handler().post {
            myDataset?.add(null)
            notifyItemInserted(myDataset?.size!! - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        if (myDataset?.size != 0) {
            myDataset?.removeAt(myDataset?.size!! - 1)
            notifyItemRemoved(myDataset!!.size)
        }
    }

    fun removeItems(position: Int) {
        //Remove loading item
        if (myDataset?.size != 0) {
            myDataset?.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, myDataset!!.size)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.items_list_product, parent, false)
            ItemViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(context).inflate(R.layout.progress_loading, parent, false)
            LoadingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return myDataset!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (myDataset!![position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {

            val mGoodsData = myDataset!![position] as ProductData

            if (!mGoodsData.remark.isNullOrEmpty()) {
                holder.itemView.titleReceiptNo?.visibility = View.VISIBLE
                val title =
                    if (!mGoodsData.remark.isNullOrEmpty()) "Receipt No: ${mGoodsData.remark}" else ""
                holder.itemView.titleReceiptNo.text = "$title"
            }

            if (!mGoodsData?.customer_name.isNullOrBlank()) {
                holder.itemView.tvCustomerName?.visibility = View.VISIBLE
                val mCustomerName =
                    if (!mGoodsData.customer_name.isNullOrEmpty()) "${mGoodsData.customer_name}" else ""
                holder.itemView.tvCustomerName.text = "$mCustomerName"
            }

            if (!mGoodsData?.code_id.isNullOrBlank()) {
                holder.itemView.tvCustomerCode?.visibility = View.VISIBLE
                val customerCode =
                    if (!mGoodsData?.code_id.isNullOrBlank()) "${mGoodsData?.code_id} " else ""
                holder.itemView.tvCustomerCode.text = "$customerCode"
            }

            if (!mGoodsData.kind_of_goods.isNullOrEmpty()) {
                holder.itemView?.tvName?.visibility = View.VISIBLE
                holder.itemView?.tvName?.text =
                    "${context.getString(R.string.goods)} : ${mGoodsData.kind_of_goods}"
            }

            if (!mGoodsData.quantity.isNullOrEmpty() || !mGoodsData.weight.isNullOrEmpty() || !mGoodsData.width.isNullOrEmpty() || !mGoodsData.length.isNullOrEmpty()) {
                val qty =
                    if (!mGoodsData.quantity.isNullOrEmpty()) "${context.getString(R.string.qty)} : ${mGoodsData.quantity}" else ""

                val width =
                    if (!mGoodsData.width.isNullOrEmpty()) " W: ${mGoodsData.width}" else ""
                val height =
                    if (!mGoodsData.height.isNullOrEmpty()) " H: ${mGoodsData.height}" else ""
                val length =
                    if (!mGoodsData.length.isNullOrEmpty()) " L: ${mGoodsData.length}" else ""


                val resultQtyWieght =
                    if (!mGoodsData.quantity.isNullOrEmpty()) "$qty" else ""

                holder.itemView?.tv_qty_wieght?.visibility = View.VISIBLE
                holder.itemView?.tv_qty_wieght?.text = "$resultQtyWieght $width $length $height"
            }
            if (!mGoodsData.weight.isNullOrEmpty()) {
                holder.itemView?.tv_wieght?.visibility = View.VISIBLE
                val wieght =
                    if (!mGoodsData.weight.isNullOrEmpty()) " ${context.getString(R.string.weight)}: ${mGoodsData.weight}" else ""
                holder.itemView?.tv_wieght?.text = "$wieght"
            }

            if (!mGoodsData.loading_date.isNullOrEmpty() || !mGoodsData.title.isNullOrEmpty()) {
                val mDateTimeShow =
                    if (!mGoodsData.loading_date.isNullOrEmpty()) "${context.getString(R.string.date)} ${mGoodsData.loading_date} ${mGoodsData.title}" else ""

                holder.itemView?.tv_editer?.text = "$mDateTimeShow"
            }

            holder.itemView?.setOnClickListener {
                if (mItemListenter != null) {
                    mItemListenter?.onIntent(myDataset!![position]!!, position)
                }
            }
        }

    }

    interface itemListenter {
        fun onIntent(data: ProductData, position: Int)
    }
}