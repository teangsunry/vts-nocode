package vts.app.china.vtsservice

import android.content.Context
import android.text.TextUtils
import retrofit2.Retrofit
import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import vts.app.china.utils.Const
import okhttp3.logging.HttpLoggingInterceptor
import vts.app.china.utils.PrefUtils
import java.util.concurrent.TimeUnit
import com.google.gson.GsonBuilder

/**
 * TEANG SUNRY 21-01-2020
 */

class ApiClient {

    private var retrofit: Retrofit? = null
    private val REQUEST_TIMEOUT = 1
    private var okHttpClient: OkHttpClient? = null

    fun getClient(context: Context): Retrofit {

        if (okHttpClient == null)
            initOkHttp(context)

        if (retrofit == null) {
            val gson = GsonBuilder()
                .setLenient()
                .create()

            retrofit = Retrofit.Builder()
                .baseUrl(Const.BASE_URL)
                .client(okHttpClient!!)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return retrofit!!
    }

    private fun initOkHttp(context: Context) {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.MINUTES)
            .readTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.MINUTES)
            .writeTimeout(REQUEST_TIMEOUT.toLong(), TimeUnit.MINUTES)


        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor(interceptor)

        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json; charset=utf-8")

            if (!TextUtils.isEmpty(PrefUtils.getApiKey(context))) {
                requestBuilder.addHeader("Authorization", PrefUtils?.getApiKey(context))
            }
            if (PrefUtils.getLoginData(context) != null && !TextUtils.isEmpty(
                    PrefUtils.getLoginData(
                        context
                    )?.username
                )
            ) {
                requestBuilder.addHeader("username", "${PrefUtils.getLoginData(context)?.username}")
            }
            val request = requestBuilder.build()
            chain.proceed(request)
        }
        okHttpClient = httpClient.build()
    }
}