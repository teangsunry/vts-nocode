package vts.app.china.vtsservice

import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*
import vts.app.china.model.response.*


interface ApiInterface {

    @FormUrlEncoded
    @POST("login/app_nocode")
    fun loginApi(
        @Field("username") username: String,
        @Field("password") password: String
    ): Observable<LoginResponse>

    // GET WAREHOUSE LIST
    @GET("warehuse_nocode")
    fun warehoustList(): Observable<WareHouseData>

    // GET CONTAINER LIST
    @GET("warehuse_nocode/group_container")
    fun getContainerList(
        @Query("ware_house_title") ware_house_title: String,
        @Query("page") page: String

    ): Observable<ContainerResponse>

    @FormUrlEncoded
    @POST("warehuse_nocode/search")
    fun productList(
        @Field("ware_house_title") ware_house_title: String,
        @Field("con_no_id") con_no_id: String,
        @Field("search") mQueryText: String,
        @Field("page") page: String

    ): Observable<ProductResponse>

    @Multipart
    @POST("warehuse_nocode/save_nocode")
    fun saveData(
        @Part userfile: Array<MultipartBody.Part>,
        @Part dataOrdering: ArrayList<MultipartBody.Part>
    ): Observable<SaveDataResponse>

    @Multipart
    @POST("warehuse_nocode/save_nocode")
    fun saveDataNoImage(
        @Part userfile: MultipartBody.Part,
        @Part dataOrdering: ArrayList<MultipartBody.Part>
    ): Observable<SaveDataResponse>

    //=======================================
    /**
     *
     *
     * BELOW IS REPORT BLOCK COD
     *
     *
     */

    // GET REPORT WAREHOUSE LIST
    @GET("warehuse_nocode/get_history_nocode")
    fun warehouseReportList(@Query("clear") clear: String): Observable<WareHouseData>

    @GET("warehuse_nocode/get_history_nocode_clear")
    fun warehouseReportClearList(): Observable<WareHouseData>

    // GET REPORT REPORT CONTAINER LIST
    @GET("warehuse_nocode/get_container_history_nocode")
    fun getContainerReportList(
        @Query("ware_house_title") ware_house_title: String,
        @Query("page") page: String

    ): Observable<ContainerResponse>


    // GET REPORT LIST
    @FormUrlEncoded
    @POST("warehuse_nocode/get_hostory_list")
    fun reportProductList(
        @Field("clear") clear: String = "",
        @Field("ware_house_title") ware_house_title: String,
        @Field("con_no_id") con_no_id: String,
        @Field("search") search: String,
        @Field("page") page: String

    ): Observable<ReportResponse>


    // SAVE REMARK
    @FormUrlEncoded
    @POST("warehuse_nocode/save_remark_more")
    fun saveRemark(
        @Field("remark_more") remark: String,
        @Field("id") id: String
    ): Observable<RemarkResponse>

    //UPDATE STOCK
    @FormUrlEncoded
    @POST("warehuse_nocode/save_clear")
    fun saveStock(
        @Field("clear") clear: String,
        @Field("id") id: String
    ): Observable<RemarkResponse>
}