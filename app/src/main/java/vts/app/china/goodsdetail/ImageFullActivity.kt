package vts.app.china.goodsdetail

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.image_full_screen.*
import vts.app.china.R
import vts.app.china.vtsui.BaseActivity
import vts.app.china.vtsui.adpater.ZoomOutPageTransformer

/**
 * Created By Taing Sunry on 2020-01-29.
 */

class ImageFullActivity : BaseActivity(), BaseActivity.CalendarToolbarListener {

    private var mImageArray = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.image_full_screen)

        val toolbar: Toolbar = findViewById(R.id.toolbar_imageFullScreen)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        UiData()

    }

    private inner class ScreenSlidePagerAdapter(
        fm: FragmentManager,
        var mImageArrays: ArrayList<String> = arrayListOf()
    ) :
        FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = mImageArrays?.size

        override fun getItem(position: Int): Fragment =
            ImageFullFragment.newInstance(mImageArrays[position])
    }

    // set data detail
    private fun UiData() {
        mImageArray = intent.getStringArrayListExtra("data")
        var mPosition = intent?.getIntExtra("position", 0)
        pagerImageFullImage?.adapter = ScreenSlidePagerAdapter(supportFragmentManager, mImageArray)

        pagerImageFullImage?.setPageTransformer(true, ZoomOutPageTransformer())
        pagerImageFullImage?.currentItem = mPosition!!.toInt()
        text_imgCount?.text = "${pagerImageFullImage?.currentItem?.plus(1)}/${mImageArray?.size}"
        pagerImageFullImage?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                text_imgCount?.text =
                    "${mImageArray?.size}/${pagerImageFullImage?.currentItem?.plus(1)}"
            }
        })

    }

    override fun onToolbarCalender(isShow: Boolean) {
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}