package vts.app.china.goodsdetail

import android.app.Activity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import vts.app.china.R
import vts.app.china.vtsui.adpater.ReviseGoodsAdapter
import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.text.TextUtils
import androidx.appcompat.widget.SearchView
import kotlinx.android.synthetic.main.activity_revise_goods.*
import vts.app.china.presenter.ReportListPresenter
import vts.app.china.vtsview.ReportListInterface
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import vts.app.china.recylerview_infinit_scroll.OnLoadMoreListener
import vts.app.china.recylerview_infinit_scroll.RecyclerViewLoadMoreScroll
import vts.app.china.vtsui.BaseActivity
import androidx.core.view.MenuItemCompat
import vts.app.china.model.response.*
import androidx.recyclerview.widget.ItemTouchHelper
import com.google.android.material.snackbar.Snackbar
import vts.app.china.utils.NetworkUtils
import vts.app.china.utils.RecyclerItemTouchHelper


/**
 * Created By Taing Sunry on 2020-02-08.
 */

class GoodsListActivity : BaseActivity(), ReviseGoodsAdapter.itemListenter,
    ReportListInterface.reportView, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var reportPresenter: ReportListInterface.reportPresent
    private lateinit var viewAdapter: ReviseGoodsAdapter
    lateinit var scrollListener: RecyclerViewLoadMoreScroll

    private var mDataList = arrayListOf<ReportListData?>()
    private var page: Int = 1
    private var mDataItems: ContainerData? = null
    private var updatePosistion = 0
    private var con_code_id: String = ""
    private var title_warehouse: String = ""
    private var textSearch: String = ""
    private var isResultCode = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_revise_goods)
        isResultCode = Activity.RESULT_CANCELED

        setPresenter(ReportListPresenter(this, this))

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        mDataItems = intent.getSerializableExtra("data") as ContainerData
        con_code_id = "${mDataItems?.con_no_id}"
        title_warehouse = "${mDataItems?.ware_house_title}"

        supportActionBar?.title = title_warehouse
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setUpList()

        requestData(
            ware_house_title = "${mDataItems?.ware_house_title}",
            con_no_id = "$con_code_id",
            search = "$textSearch",
            page = "$page",
            isMore = false
        )

        swipeRefresh?.setOnRefreshListener {
            page = 1
            requestData(
                ware_house_title = "${mDataItems?.ware_house_title}",
                con_no_id = "$con_code_id",
                search = "$textSearch",
                page = "$page",
                isMore = false
            )
        }

    }

    private fun requestData(
        ware_house_title: String,
        con_no_id: String,
        search: String,
        page: String,
        isMore: Boolean
    ) {
        if (!isMore) {
            this.page = 1
            swipeRefresh?.isRefreshing = true
        }
        reportPresenter.onLoadProductListReport(
            clear = "",
            ware_house_title = ware_house_title,
            con_no_id = con_no_id,
            search = search,
            page = page,
            isMore = isMore
        )
    }

    override fun onError(throwable: Throwable) {
        tvNoDataFound?.visibility = View.VISIBLE
        swipeRefresh?.isRefreshing = false
    }

    override fun setPresenter(presenter: ReportListInterface.reportPresent) {
        reportPresenter = presenter
    }

    override fun onError(t: Any) {
        swipeRefresh?.isRefreshing = false
    }

    override fun onErrorConnection(any: Any) {
        swipeRefresh?.isRefreshing = false
    }

    override fun onIntent(data: ReportListData, position: Int) {
        updatePosistion = position
        val intentDetail = Intent(this, GoodsDetailActivity::class.java)
        intentDetail.putExtra("data", data)
        startActivityForResult(intentDetail, 1029)
    }

    private fun setUpList() {
        viewManager = LinearLayoutManager(this)
        viewAdapter = ReviseGoodsAdapter(mDataList!!, this, false, this)
        recyclerView = findViewById<RecyclerView>(R.id.mRecylerListGoods).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        setRVScrollListener()
        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int, position: Int) {
        if (viewHolder is ReviseGoodsAdapter.ItemViewHolder) {
            viewHolder?.viewBackground?.findViewById<TextView>(R.id.tvClearIng)?.visibility =
                View.GONE
            viewHolder?.viewBackground?.findViewById<RelativeLayout>(R.id.startLoading)
                ?.visibility = View.VISIBLE
            if (!mDataList.isNullOrEmpty()) {
                if (!NetworkUtils.isOnline(this)) {
                    val snackbar = Snackbar
                        .make(
                            coordinatorLayoutHistory,
                            "Check your internet connection!.",
                            Snackbar.LENGTH_LONG
                        )
                    snackbar.show()
                    return
                }
                val mDataLists = viewAdapter!!.getItemAtPosition(position) as ReportListData
                reportPresenter?.clearStock("1", "${mDataLists?.id}", position)
            }

        }
    }

    override fun onClearStockSuccess(responseData: RemarkResponse, position: Int) {
        if (responseData?.list_nocode.id.isNullOrBlank()) {
            Toast.makeText(this, "Update false", Toast.LENGTH_SHORT).show()
            val textError = recyclerView?.findViewHolderForAdapterPosition(position)
                ?.itemView?.findViewById<TextView>(R.id.delete_icon)
            val loadProgress = recyclerView?.findViewHolderForAdapterPosition(position)
                ?.itemView?.findViewById<ProgressBar>(R.id.progressBarClear)
            textError?.text = "Something error..."
            loadProgress?.visibility = View.GONE
            return
        }
        viewAdapter.removeItem(position)
        isResultCode = Activity.RESULT_OK
        val snackbar = Snackbar
            .make(
                coordinatorLayoutHistory,
                responseData?.list_nocode.kind_of_goods + "clear from stock!",
                Snackbar.LENGTH_LONG
            )
        snackbar.setActionTextColor(Color.YELLOW)
        snackbar.show()
//        println("CountL:${viewAdapter?.itemCount}")
        if (viewAdapter?.itemCount == 0) {
            tvNoDataFound?.visibility = View.VISIBLE
            swipeRefresh?.isRefreshing = false
        }
    }

    override fun onErrorClearStockModel(throwable: Throwable) {
        val snackbar = Snackbar
            .make(
                coordinatorLayoutHistory,
                "Server error!.",
                Snackbar.LENGTH_LONG
            )
        snackbar.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        println("result:$requestCode , $resultCode, $updatePosistion")
        if (requestCode == 1029 && resultCode == Activity.RESULT_OK) {
            val remarkMore = data?.getStringExtra("data")?.toString()
            viewAdapter?.updateObjectData("$remarkMore", updatePosistion)
        }
    }

    private fun setRVScrollListener() {
        scrollListener = RecyclerViewLoadMoreScroll(viewManager as LinearLayoutManager)
        scrollListener.setOnLoadMoreListener(object :
            OnLoadMoreListener {
            override fun onLoadMore() {
                viewAdapter?.addLoadingView()
                requestData(
                    ware_house_title = "${mDataItems?.ware_house_title}",
                    con_no_id = "$con_code_id",
                    search = "$textSearch",
                    page = "$page",
                    isMore = true
                )

            }
        })
        recyclerView.addOnScrollListener(scrollListener)
    }

    override fun onSuccess(responseData: ReportResponse) {
        mDataList?.clear()
        mDataList = responseData!!.list_nocode as ArrayList<ReportListData?>
        viewAdapter?.notifyDataSetChanged()
        viewAdapter?.updateData(mDataList)

        swipeRefresh?.isRefreshing = false
        var view = if (mDataList?.size == 0) View.VISIBLE else View.GONE
        tvNoDataFound?.visibility = view
        page++
        scrollListener?.setLoaded()
    }


    override fun onSuccessModelMore(responseData: ReportResponse) {
        viewAdapter?.removeLoadingView()
        mDataList = responseData!!.list_nocode as ArrayList<ReportListData?>
        swipeRefresh?.isRefreshing = false
        if (mDataList?.size == 0) {
            return
        }
        page++
        viewAdapter?.addData(mDataList)
        scrollListener?.setLoaded()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchViewItem = menu?.findItem(R.id.action_master_search)
        val searchViewAndroidActionBar = MenuItemCompat.getActionView(searchViewItem) as SearchView

        if (con_code_id.isNullOrBlank()) {
            searchViewItem?.expandActionView()
        }

        searchViewAndroidActionBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchViewAndroidActionBar?.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                textSearch = "$newText"
                page = 1
                requestData(
                    ware_house_title = title_warehouse,
                    con_no_id = con_code_id,
                    search = "$textSearch",
                    page = "$page",
                    isMore = false
                )
                return true
            }

        })
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(isResultCode)
        super.onBackPressed()
    }

}