package vts.app.china.goodsdetail

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.activity_goods_detail.*
import vts.app.china.R
import vts.app.china.model.response.ReportListData
import vts.app.china.vtsui.BaseActivity
import vts.app.china.vtsui.adpater.ZoomOutPageTransformer
import android.view.View
import vts.app.china.vtsui.fragment.AddRemarkFragmentDialog
import androidx.fragment.app.DialogFragment
import androidx.viewpager.widget.ViewPager
import vts.app.china.model.response.RemarkResponse


/**
 * Created By Taing Sunry on 2020-01-29.
 */

class GoodsDetailActivity : BaseActivity(), BaseActivity.CalendarToolbarListener,
    AddRemarkFragmentDialog.OnFragmentInteractionListener {

    private var mData: ReportListData? = null
    private var mImageArray = arrayListOf<String>()
    private var remarkPosition = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_goods_detail)
        remarkPosition = Activity.RESULT_CANCELED
        val toolbar: Toolbar = findViewById(R.id.toolbar_goods_detail)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mData = intent.getSerializableExtra("data") as ReportListData
        UiData()

        onCollapsingToolbarListener(app_bar, toolbar_layout, "${mData?.kind_of_goods}", this)

        fbMoreRemark?.setOnClickListener {
            val ft = supportFragmentManager.beginTransaction()
            val showDialogFrag: DialogFragment =
                AddRemarkFragmentDialog("${mData?.id}", "${mData?.remark_more}")
            showDialogFrag.show(ft, "show")
        }

        mImageArray?.clear()
        val items = mData?.prod_image?.split(",")
        if (items?.size != 0) {
            items?.forEach {
                if (!it.isNullOrBlank())
                    mImageArray.add("${mData?.part}${it}")

            }
        }
        pagerImage?.adapter = ScreenSlidePagerAdapter(supportFragmentManager, mImageArray)

        pagerImage?.setPageTransformer(true, ZoomOutPageTransformer())
        if (mImageArray?.size > 0) {
            imageCounter?.visibility = View.VISIBLE
        } else {
            imageNoImage?.visibility = View.VISIBLE
        }
        imageCounter?.text = "${pagerImage?.currentItem?.plus(1)}/${mImageArray?.size}"
        pagerImage?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                imageCounter?.text =
                    "${mImageArray?.size}/${pagerImage?.currentItem?.plus(1)}"
            }
        })

    }

    private inner class ScreenSlidePagerAdapter(
        fm: FragmentManager,
        var mImageArrays: ArrayList<String> = arrayListOf()
    ) :
        FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = mImageArrays?.size

        override fun getItem(position: Int): Fragment =
            GoodsImageFragment.newInstance(
                mImageArrays[position],
                position,
                mImageArrays
            )
    }

    // set data detail
    private fun UiData() {

        val customerID =
            if (!mData?.con_id.isNullOrEmpty()) "-${mData?.con_id}" else ""

        val forwarderCode =
            if (!mData?.con_no_id.isNullOrEmpty()) "-${mData?.con_no_id}" else ""

        val title =
            if (!mData?.customer_name.isNullOrEmpty()) "${mData?.customer_name}$customerID" else "${mData?.con_id}"

        titleProduct?.text = "$title $forwarderCode"
        if (!mData?.con_id.isNullOrBlank()) {
            text_costomerCode?.visibility = View.VISIBLE
            text_costomerCode?.text = "${getString(R.string.code_id)}: ${mData?.con_id}"
        }

        if (!mData?.kind_of_goods.isNullOrBlank()) {
            text_kind_of_goods?.visibility = View.VISIBLE
            text_kind_of_goods?.text = "${getString(R.string.goods)}: ${mData?.kind_of_goods}"
        }

        if (!mData?.quantity.isNullOrBlank()) {
            text_quantity?.visibility = View.VISIBLE
            text_quantity?.text = "${getString(R.string.qty)}: ${mData?.quantity}"
        }

        if (!mData?.weight.isNullOrBlank()) {
            text_wieght?.visibility = View.VISIBLE
            text_wieght?.text = "${getString(R.string.weight)}: ${mData?.weight}"
        }

        if (!mData?.width.isNullOrBlank()) {
            text_width?.visibility = View.VISIBLE
            text_width?.text = "${getString(R.string.width)}: ${mData?.width}"
        }

        if (!mData?.height.isNullOrBlank()) {
            text_hiegth?.visibility = View.VISIBLE
            text_hiegth?.text = "${getString(R.string.height)}: ${mData?.height}"
        }

        if (!mData?.length.isNullOrBlank()) {
            text_length?.visibility = View.VISIBLE
            text_length?.text = "${getString(R.string.length)}: ${mData?.length}"
        }

        if (!mData?.remark.isNullOrBlank()) {
            text_remark?.visibility = View.VISIBLE
            text_remark?.text = "${getString(R.string.remark)}: ${mData?.remark}"
        }

        if (!mData?.remark_more.isNullOrBlank()) {
            text_remark_more?.visibility = View.VISIBLE
            text_remark_more?.text = "Remark more:${mData?.remark_more}"
        }

        if (!mData?.receip_no.isNullOrBlank()) {
            reciept_no?.visibility = View.VISIBLE
            reciept_no?.text = "Receipt No: ${mData?.receip_no}"
        }

        if (!mData?.loading_date.isNullOrBlank() || !mData?.title.isNullOrBlank()) {
            text_loading_date?.visibility = View.VISIBLE
            text_loading_date?.text =
                "${getString(R.string.date)}: ${mData?.loading_date}${mData?.title}"
        }

        if (!mData?.remark_no.isNullOrBlank() ) {
            remark_no?.visibility = View.VISIBLE
            remark_no?.text = "Remark No: ${mData?.remark_no}"
        }


        if (!mData?.created_time.isNullOrBlank() || !mData?.user.isNullOrBlank()) {
            val user = if (!mData?.user.isNullOrBlank()) "User:${mData?.user} " else ""
            text_created_time?.visibility = View.VISIBLE
            text_created_time?.text =
                "$user${getString(R.string.c_date)}: ${mData?.created_time}"
        }

    }

    override fun onFragmentInteraction(responseData: RemarkResponse) {
        mData = responseData.list_nocode
        remarkPosition = Activity.RESULT_OK
        UiData()
    }

    override fun onToolbarCalender(isShow: Boolean) {
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(remarkPosition, intent.putExtra("data", mData?.remark_more))
        super.onBackPressed()

    }
}