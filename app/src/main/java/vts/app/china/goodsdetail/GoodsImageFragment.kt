package vts.app.china.goodsdetail

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_image_view.view.*
import vts.app.china.R
import java.lang.Exception


/**
 * Created By Taing Sunry on 2020-02-14.
 */

class GoodsImageFragment : Fragment() {
    private var mData: String? = ""
    private var position: Int? = 0

    companion object {
        fun newInstance(
            mData: String,
            position: Int,
            mDataImage: ArrayList<String>
        ): GoodsImageFragment {

            val frag = GoodsImageFragment()
            val args = Bundle()
            args.putString("data", mData)
            args.putInt("position", position)
            args.putStringArrayList("img[]", mDataImage)
            frag.arguments = args
            return frag
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val goodsView =
            LayoutInflater.from(context).inflate(R.layout.item_image_view, container, false)
        mData = if (arguments != null) arguments?.get("data")?.toString() else ""
        position = if (arguments != null) arguments?.getInt("position") else 0
//        println("url:$mData")
        Picasso.get()
            .load("$mData")
            .into(goodsView!!.imageViewPhoto, object : Callback {
                override fun onSuccess() {
                    goodsView?.loadinImage?.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    goodsView?.loadinImage?.visibility = View.GONE
                    goodsView!!.imageViewPhoto?.setImageResource(R.drawable.error_image)
                }
            })

        goodsView?.imageViewPhoto?.setOnClickListener {
            val mIntentImageFull = Intent(activity!!, ImageFullActivity::class.java)
            mIntentImageFull.putExtra(
                "data",
                arguments?.getStringArrayList("img[]") as ArrayList<String>
            )
            mIntentImageFull.putExtra("position", position)
            startActivity(mIntentImageFull)
        }
        return goodsView
    }
}