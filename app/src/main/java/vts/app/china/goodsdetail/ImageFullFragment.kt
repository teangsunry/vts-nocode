package vts.app.china.goodsdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.full_image_item.view.*
import vts.app.china.R


/**
 * Created By Taing Sunry on 2020-02-14.
 */

class ImageFullFragment : Fragment() {

    private var mData: String? = ""

    companion object {
        fun newInstance(
            mData: String
        ): ImageFullFragment {
            val frag = ImageFullFragment()
            val args = Bundle()
            args.putString("data", mData)
            frag.arguments = args
            return frag
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val goodsView =
            LayoutInflater.from(context).inflate(R.layout.full_image_item, container, false)
        mData = if (arguments != null) arguments?.get("data")?.toString() else ""
        val viewImage: ImageView = goodsView!!.imageViewZoom
        Picasso.get()
            .load("$mData")
            .into(viewImage, object : Callback {
                override fun onSuccess() {
                    goodsView?.loadinImage?.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    goodsView?.loadinImage?.visibility = View.GONE
                    viewImage.setImageResource(R.drawable.error_image)
                }
            })

        return goodsView
    }
}